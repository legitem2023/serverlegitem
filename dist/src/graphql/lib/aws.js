import { createClient } from '@supabase/supabase-js';
const supabaseUrl = 'https://tsbriguuaznlvwbnylop.supabase.co';
const supabaseKey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InRzYnJpZ3V1YXpubHZ3Ym55bG9wIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MjY5MDIzOTksImV4cCI6MjA0MjQ3ODM5OX0.oKpulUfQth5hNyZVRgw_uPBnkhrcD1LP61CPmW3U-gA";
const supabase = createClient(supabaseUrl, supabaseKey);
// export const uploadFileToAmazons3 = async (filename: string, stream: Stream) => {
//     try {
//         const pathName = `images/Legit/${filename}`;
//         // streamToBuffer(stream).then(async (buffer) => {
//             const params:any = {
//                 Bucket: process.env.AWS_S3_BUCKET_NAME,
//                 Key: pathName,
//                 Body: stream
//             }
//             const results = await s3Client.send(new PutObjectCommand(params));
//             return results;
//         // })
//     } catch (error) {
//         console.log("Error: ", error)
//     }
// }
export const uploadFileToSupabaseGLB = async (filename, stream, dir, subDIR) => {
    try {
        const pathName = `${subDIR}/${filename}`;
        const { data, error } = await supabase.storage.from(dir) // Replace with your Supabase bucket name
            .upload(pathName, stream, {
            contentType: 'model/gltf-binary',
            upsert: true,
        });
        if (error) {
            throw error;
        }
        return data;
    }
    catch (error) {
        console.log('Error: ', error);
    }
};
export const uploadFileToSupabase = async (filename, stream, dir, subDIR) => {
    try {
        const pathName = `${subDIR}/${filename}`;
        const { data, error } = await supabase.storage.from(dir) // Replace with your Supabase bucket name
            .upload(pathName, stream, {
            contentType: 'image/jpeg',
            upsert: true,
        });
        if (error) {
            throw error;
        }
        return data;
    }
    catch (error) {
        console.log('Error: ', error);
    }
};
const streamToBuffer = async (stream) => {
    return new Promise((resolve, reject) => {
        const _buf = [];
        stream.on('data', (chunk) => _buf.push(chunk));
        stream.on('end', () => resolve(Buffer.concat(_buf)));
        stream.on('error', (err) => reject(err));
    });
};
export const generateTrackingNumber = () => {
    const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    let trackingNumber = '';
    for (let i = 0; i < 12; i++) {
        trackingNumber += "TRK-" + chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return trackingNumber;
};
export const generateOrderNumber = () => {
    const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    let orderNumber = 'ODR-'; // Prefix added once before the loop
    for (let i = 0; i < 10; i++) { // Reduced to 8 random characters
        orderNumber += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return orderNumber;
};
