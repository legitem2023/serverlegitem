import { PrismaClient } from "@prisma/client";
import bcrypt from "bcrypt";
import jwt from 'jsonwebtoken';
import https from 'https';
import { encode } from "js-base64";
import { generateTrackingNumber, generateOrderNumber, uploadFileToSupabase, uploadFileToSupabaseGLB } from "./lib/aws.js";
import { PubSub } from "graphql-subscriptions";
import { ContactUs, createLinkURL, sendMail } from "../../utils/script.js";
import { format } from 'date-fns';
const pubsub = new PubSub();
const MESSAGE_ADDED = [];
const MESSAGE_COMMENT = [];
const MESSAGE_TO_ORDER = [];
const MESSAGE_NEWS = [];
const ACTIVE_USERS = [];
const MESSAGE_PERSONAL = [];
const CALL_RECEIVED = 'CALL_RECEIVED';
const ICE_CANDIDATE_RECEIVED = 'ICE_CANDIDATE_RECEIVED';
const Active = [];
const SCOPE = ['https://www.googleapis.com/auth/drive'];
const prisma = new PrismaClient();
export const resolvers = {
    Query: {
        readSales: async (_parent, args) => {
            try {
                const data = await prisma.orderHistory.findMany();
                const parameter = args.period;
                const period = [{
                        "period": "yyyy-MM-dd",
                        "name": "Daily"
                    },
                    {
                        "period": "yyyy-ww",
                        "name": "Weekly"
                    },
                    {
                        "period": "yyyy-MM",
                        "name": "Monthly"
                    },
                    {
                        "period": "yyyy",
                        "name": "Yearly"
                    }
                ];
                const interval = period
                    .filter((item) => item.name === parameter)
                    .map((item) => ({ period: item.period }));
                const groupedByWeek = data.reduce((acc, item) => {
                    const IntervalName = format(new Date(item.dateCreated), interval[0].period);
                    acc[IntervalName] = acc[IntervalName] || [];
                    acc[IntervalName].push(item);
                    return acc;
                }, {});
                const result = Object.entries(groupedByWeek).map(([IntervalName, sales]) => ({
                    Interval: IntervalName,
                    totalSales: sales.reduce((sum, sale) => sum + (sale.Price * sale.Quantity), 0),
                    orderHistory: sales // The actual sales data for that week
                }));
                return result;
            }
            catch (error) {
            }
        },
        readLikes: async (_parent, args) => {
            try {
                return await prisma.childInventory.findMany({
                    where: {
                        Likes: {
                            some: {
                                accountEmail: args === null || args === void 0 ? void 0 : args.accountEmail,
                                productCode: args === null || args === void 0 ? void 0 : args.productCode, // Filter by product codes in Likes
                            }
                        },
                    },
                    include: {
                        Likes: true, // Include Likes in the result
                    }
                });
            }
            catch (error) {
                console.log(error);
            }
        },
        readRatings: async (_parent, args) => {
            var _a;
            try {
                const result = await prisma.productFeedBacks.aggregate({
                    where: {
                        productCode: args.productCodes,
                    },
                    _sum: {
                        Ratings: true, // Sum the ratings field
                    },
                });
                const result_B = await prisma.productFeedBacks.findMany({
                    where: {
                        productCode: args.productCodes,
                    }
                });
                const sumRatings = (_a = result === null || result === void 0 ? void 0 : result._sum) === null || _a === void 0 ? void 0 : _a.Ratings;
                const count = result_B.length;
                const avgRating = sumRatings / count;
                return avgRating;
            }
            catch (error) {
                console.log(error);
            }
        },
        readFeedBack: async (_parent, args_) => {
            try {
                return await prisma.productFeedBacks.findMany();
            }
            catch (error) {
                console.log(error);
            }
        },
        readPrivacy: async (_parent, args_) => {
            try {
                return await prisma.privacy.findMany();
            }
            catch (error) {
                console.log(error);
            }
        },
        readDisclaimer: async (_parent, args_) => {
            try {
                return await prisma.disclaimer.findMany();
            }
            catch (error) {
                console.log(error);
            }
        },
        readAbout: async (_parent, args_) => {
            try {
                return await prisma.about.findMany();
            }
            catch (error) {
                console.log(error);
            }
        },
        readFAQ: async (_parent, args_) => {
            try {
                return await prisma.fAQ.findMany();
            }
            catch (error) {
                console.log(error);
            }
        },
        readActiveUsers: async (_parent, args_, context) => {
            try {
                const { cookies } = context;
                const authToken = cookies['clientToken'];
                let Activated = [];
                let minimize = [];
                if (!authToken) {
                    throw new Error('User is not authenticated');
                }
                // Decode the URL-encoded array of tokens
                const decodedTokens = JSON.parse(decodeURIComponent(authToken));
                const jwt_token = process.env.JWT_ACCESS_TOKEN_SECRET;
                // Iterate over the array of tokens and decode each one
                for (const token of decodedTokens) {
                    const decoded = jwt.decode(token, jwt_token);
                    if (!decoded || !decoded.user) {
                        throw new Error('Invalid token');
                    }
                    // Add the active user to the list
                    Activated.push({
                        accountEmail: decoded.user.emailAddress,
                        fullname: decoded.user.emailAddress,
                    });
                }
                pubsub.publish(ACTIVE_USERS, { ActiveUserList: Activated });
                return Activated;
            }
            catch (error) {
                return [{
                        "accountEmail": "No Active User",
                        "fullname": "No Active User",
                    }];
            }
        },
        readNewsManagement: async (_parent, args) => {
            try {
                return await prisma.news.findMany({
                    where: {
                        postedBy: args.emailAddress
                    }
                });
            }
            catch (error) {
                console.log(error);
            }
        },
        readNews: async (_parent, args_) => {
            const messages = await prisma.news.findMany();
            pubsub.publish(MESSAGE_NEWS, { messageNews: messages });
            return messages;
        },
        readNewsPoster: async (_parent, args_) => {
            try {
                return await prisma.news.findMany({
                    distinct: ["postedBy"]
                });
            }
            catch (error) {
                console.log(error);
            }
        },
        readGroupedOrderHistory: async (_parent, args) => {
            const result = await prisma.orderHistory.findMany({
                distinct: ['OrderNo'],
                where: {
                    agentEmail: args.emailAddress,
                    OrderStatus: 'New Order'
                },
                orderBy: {
                    dateCreated: 'desc',
                }
            });
            const child = await prisma.orderHistory.findMany({
                where: {
                    agentEmail: args.emailAddress,
                    OrderStatus: 'New Order'
                },
                orderBy: {
                    dateCreated: 'desc',
                }
            });
            const result2 = result.map((item) => {
                return {
                    OrderNo: item.OrderNo,
                    Address: item.Address,
                    Contact: item.Contact,
                    StatusText: item.StatusText,
                    OrderStatus: item.OrderStatus,
                    OrderHistory: child.filter((childItem) => childItem.OrderNo === item.OrderNo),
                };
            });
            return result2;
        },
        readGroupedOrderHistoryRecieved: async (_parent, args) => {
            const result = await prisma.orderHistory.findMany({
                distinct: ['OrderNo'],
                where: {
                    agentEmail: args.emailAddress,
                    OrderStatus: 'Recieved'
                },
                orderBy: {
                    dateCreated: 'desc',
                }
            });
            const child = await prisma.orderHistory.findMany({
                where: {
                    agentEmail: args.emailAddress,
                    OrderStatus: 'Recieved'
                },
                orderBy: {
                    dateCreated: 'desc',
                }
            });
            const result2 = result.map((item) => {
                return {
                    OrderNo: item.OrderNo,
                    Address: item.Address,
                    Contact: item.Contact,
                    StatusText: item.StatusText,
                    OrderStatus: item.OrderStatus,
                    OrderHistory: child.filter((childItem) => childItem.OrderNo === item.OrderNo),
                };
            });
            return result2;
        },
        readGroupedOrderHistoryPacked: async (_parent, args) => {
            const result = await prisma.orderHistory.findMany({
                distinct: ['OrderNo'],
                where: {
                    agentEmail: args.emailAddress,
                    OrderStatus: 'Packed'
                },
                orderBy: {
                    dateCreated: 'desc',
                }
            });
            const child = await prisma.orderHistory.findMany({
                where: {
                    agentEmail: args.emailAddress,
                    OrderStatus: 'Packed'
                },
                orderBy: {
                    dateCreated: 'desc',
                }
            });
            const result2 = result.map((item) => {
                return {
                    OrderNo: item.OrderNo,
                    Address: item.Address,
                    Contact: item.Contact,
                    StatusText: item.StatusText,
                    OrderStatus: item.OrderStatus,
                    OrderHistory: child.filter((childItem) => childItem.OrderNo === item.OrderNo),
                };
            });
            return result2;
        },
        readGroupedOrderHistoryLogistic: async (_parent, args) => {
            const result = await prisma.orderHistory.findMany({
                distinct: ['OrderNo'],
                where: {
                    agentEmail: args.emailAddress,
                    OrderStatus: 'Logistic'
                },
                orderBy: {
                    dateCreated: 'desc',
                }
            });
            const child = await prisma.orderHistory.findMany({
                where: {
                    agentEmail: args.emailAddress,
                    OrderStatus: 'Logistic'
                },
                orderBy: {
                    dateCreated: 'desc',
                }
            });
            const result2 = result.map((item) => {
                return {
                    OrderNo: item.OrderNo,
                    Address: item.Address,
                    Contact: item.Contact,
                    StatusText: item.StatusText,
                    OrderStatus: item.OrderStatus,
                    OrderHistory: child.filter((childItem) => childItem.OrderNo === item.OrderNo),
                };
            });
            return result2;
        },
        readGroupedOrderHistoryDelivery: async (_parent, args) => {
            const result = await prisma.orderHistory.findMany({
                distinct: ['OrderNo'],
                where: {
                    agentEmail: args.emailAddress,
                    OrderStatus: 'Delivery'
                },
                orderBy: {
                    dateCreated: 'desc',
                }
            });
            const child = await prisma.orderHistory.findMany({
                where: {
                    agentEmail: args.emailAddress,
                    OrderStatus: 'Delivery'
                },
                orderBy: {
                    dateCreated: 'desc',
                }
            });
            const result2 = result.map((item) => {
                return {
                    OrderNo: item.OrderNo,
                    Address: item.Address,
                    Contact: item.Contact,
                    StatusText: item.StatusText,
                    OrderStatus: item.OrderStatus,
                    OrderHistory: child.filter((childItem) => childItem.OrderNo === item.OrderNo),
                };
            });
            return result2;
        },
        readGroupedOrderHistoryDelivered: async (_parent, args) => {
            const result = await prisma.orderHistory.findMany({
                distinct: ['OrderNo'],
                where: {
                    agentEmail: args.emailAddress,
                    OrderStatus: 'Delivered'
                },
                orderBy: {
                    dateCreated: 'desc',
                }
            });
            const child = await prisma.orderHistory.findMany({
                where: {
                    agentEmail: args.emailAddress,
                    OrderStatus: 'Delivered'
                },
                orderBy: {
                    dateCreated: 'desc',
                }
            });
            const result2 = result.map((item) => {
                return {
                    OrderNo: item.OrderNo,
                    Address: item.Address,
                    Contact: item.Contact,
                    StatusText: item.StatusText,
                    OrderStatus: item.OrderStatus,
                    OrderHistory: child.filter((childItem) => childItem.OrderNo === item.OrderNo),
                };
            });
            return result2;
        },
        getGroupedOrderHistory: async (_parent, args) => {
            const result = await prisma.orderHistory.findMany({
                distinct: ['OrderNo'],
                where: {
                    emailAddress: args.emailAddress,
                    OrderStatus: 'New Order'
                },
                orderBy: {
                    dateCreated: 'desc',
                }
            });
            const child = await prisma.orderHistory.findMany({
                where: {
                    emailAddress: args.emailAddress,
                    OrderStatus: 'New Order'
                },
                orderBy: {
                    dateCreated: 'desc',
                }
            });
            const result2 = result.map((item) => {
                return {
                    OrderNo: item.OrderNo,
                    Address: item.Address,
                    Contact: item.Contact,
                    StatusText: item.StatusText,
                    OrderStatus: item.OrderStatus,
                    OrderHistory: child.filter((childItem) => childItem.OrderNo === item.OrderNo),
                };
            });
            return result2;
        },
        getGroupedOrderHistoryRecieved: async (_parent, args) => {
            const result = await prisma.orderHistory.findMany({
                distinct: ['OrderNo'],
                where: {
                    emailAddress: args.emailAddress,
                    OrderStatus: 'Recieved'
                },
                orderBy: {
                    dateCreated: 'desc',
                }
            });
            const child = await prisma.orderHistory.findMany({
                where: {
                    emailAddress: args.emailAddress,
                    OrderStatus: 'Recieved'
                },
                orderBy: {
                    dateCreated: 'desc',
                }
            });
            const result2 = result.map((item) => {
                return {
                    OrderNo: item.OrderNo,
                    Address: item.Address,
                    Contact: item.Contact,
                    StatusText: item.StatusText,
                    OrderStatus: item.OrderStatus,
                    OrderHistory: child.filter((childItem) => childItem.OrderNo === item.OrderNo),
                };
            });
            return result2;
        },
        getGroupedOrderHistoryPacked: async (_parent, args) => {
            const result = await prisma.orderHistory.findMany({
                distinct: ['OrderNo'],
                where: {
                    emailAddress: args.emailAddress,
                    OrderStatus: 'Packed'
                },
                orderBy: {
                    dateCreated: 'desc',
                }
            });
            const child = await prisma.orderHistory.findMany({
                where: {
                    emailAddress: args.emailAddress,
                    OrderStatus: 'Packed'
                },
                orderBy: {
                    dateCreated: 'desc',
                }
            });
            const result2 = result.map((item) => {
                return {
                    OrderNo: item.OrderNo,
                    Address: item.Address,
                    Contact: item.Contact,
                    StatusText: item.StatusText,
                    OrderStatus: item.OrderStatus,
                    OrderHistory: child.filter((childItem) => childItem.OrderNo === item.OrderNo),
                };
            });
            return result2;
        },
        getGroupedOrderHistoryLogistic: async (_parent, args) => {
            const result = await prisma.orderHistory.findMany({
                distinct: ['OrderNo'],
                where: {
                    emailAddress: args.emailAddress,
                    OrderStatus: 'Logistic'
                },
                orderBy: {
                    dateCreated: 'desc',
                }
            });
            const child = await prisma.orderHistory.findMany({
                where: {
                    emailAddress: args.emailAddress,
                    OrderStatus: 'Logistic'
                },
                orderBy: {
                    dateCreated: 'desc',
                }
            });
            const result2 = result.map((item) => {
                return {
                    OrderNo: item.OrderNo,
                    Address: item.Address,
                    Contact: item.Contact,
                    StatusText: item.StatusText,
                    OrderStatus: item.OrderStatus,
                    OrderHistory: child.filter((childItem) => childItem.OrderNo === item.OrderNo),
                };
            });
            return result2;
        },
        getGroupedOrderHistoryDelivery: async (_parent, args) => {
            const result = await prisma.orderHistory.findMany({
                distinct: ['OrderNo'],
                where: {
                    emailAddress: args.emailAddress,
                    OrderStatus: 'Delivery'
                },
                orderBy: {
                    dateCreated: 'desc',
                }
            });
            const child = await prisma.orderHistory.findMany({
                where: {
                    emailAddress: args.emailAddress,
                    OrderStatus: 'Delivery'
                },
                orderBy: {
                    dateCreated: 'desc',
                }
            });
            const result2 = result.map((item) => {
                return {
                    OrderNo: item.OrderNo,
                    Address: item.Address,
                    Contact: item.Contact,
                    StatusText: item.StatusText,
                    OrderStatus: item.OrderStatus,
                    OrderHistory: child.filter((childItem) => childItem.OrderNo === item.OrderNo),
                };
            });
            return result2;
        },
        getGroupedOrderHistoryDelivered: async (_parent, args) => {
            const result = await prisma.orderHistory.findMany({
                distinct: ['OrderNo'],
                where: {
                    emailAddress: args.emailAddress,
                    OrderStatus: 'Delivered'
                },
                orderBy: {
                    dateCreated: 'desc',
                }
            });
            const child = await prisma.orderHistory.findMany({
                where: {
                    emailAddress: args.emailAddress,
                    OrderStatus: 'Delivered'
                },
                orderBy: {
                    dateCreated: 'desc',
                }
            });
            const result2 = result.map((item) => {
                return {
                    OrderNo: item.OrderNo,
                    Address: item.Address,
                    Contact: item.Contact,
                    StatusText: item.StatusText,
                    OrderStatus: item.OrderStatus,
                    OrderHistory: child.filter((childItem) => childItem.OrderNo === item.OrderNo),
                };
            });
            return result2;
        },
        getOrderHistory: async (_parent, args) => {
            try {
                return await prisma.orderHistory.findMany({
                    where: {
                        emailAddress: args === null || args === void 0 ? void 0 : args.emailAddress
                    }
                });
            }
            catch (error) {
                console.log(error);
            }
        },
        getInv_subImage: async (_parent, _args) => {
            try {
                return await prisma.inv_subImage.findMany();
            }
            catch (error) {
                console.log(error);
            }
        },
        getUser: async (_parent, _args) => {
            try {
                return await prisma.user.findMany({
                    include: {
                        accountDetails: true
                    }
                });
            }
            catch (error) {
                console.log(error);
            }
        },
        getFilteredUser: async (_parent, args) => {
            try {
                let result;
                const UserLevel = args.UserLevel;
                const emailAddress = args.emailAddress;
                if (UserLevel === "Merchant") {
                    result = await prisma.user.findMany({
                        where: {
                            agentIdentity: emailAddress,
                            accountLevel: UserLevel
                        },
                        include: {
                            accountDetails: true
                        }
                    });
                }
                else {
                    result = await prisma.user.findMany({
                        include: {
                            accountDetails: true
                        }
                    });
                }
                return result;
            }
            catch (error) {
                console.log(error);
            }
        },
        getAccountDetails: async (_parent, _args) => {
            try {
                const data = await prisma.accountDetails.findMany();
                return data;
            }
            catch (error) {
                console.log(error);
            }
        },
        getAccountDetails_id: async (_parent, args) => {
            var _a;
            try {
                const data = await prisma.accountDetails.findMany({
                    where: {
                        userId: (_a = args === null || args === void 0 ? void 0 : args.id) === null || _a === void 0 ? void 0 : _a.toString()
                    }
                });
                return data;
            }
            catch (error) {
                console.log(error);
            }
        },
        getParentInventory: async (_parent, args) => {
            var _a;
            try {
                const data = await prisma.inventory.findMany({
                    where: {
                        agentEmail: (_a = args === null || args === void 0 ? void 0 : args.EmailAddress) === null || _a === void 0 ? void 0 : _a.toString()
                    },
                    include: {
                        childInventory: true
                    },
                    orderBy: {
                        dateCreated: 'desc'
                    }
                });
                return data;
            }
            catch (error) {
                console.log(error);
            }
        },
        getChildInventory: async (_parent, _args) => {
            try {
                // const skip = args.skip;
                // const take = args.take;
                const data = await prisma.childInventory.findMany({
                    where: {
                        status: "Active"
                    },
                    include: {
                        Ratings: true,
                        Views: true,
                        SoldItems: true,
                        subImageFieldOut: true
                    }
                });
                // Add the sum of SoldItems' quantity for each childInventory
                const result = data.map(item => {
                    const TotalSoldItems = item.SoldItems.reduce((sum, soldItem) => sum + (soldItem === null || soldItem === void 0 ? void 0 : soldItem.Quantity), 0);
                    return Object.assign(Object.assign({}, item), { TotalSoldItems // Attach the calculated sum to the result
                     });
                });
                const resultB = result.map((item) => {
                    const Totlength = item.Ratings.length;
                    const TotalRatings = item.Ratings.reduce((sum, rating) => (sum + rating.Ratings) / Totlength, 0);
                    return Object.assign(Object.assign({}, item), { TotalRatings });
                });
                // console.log(result);
                return resultB;
            }
            catch (error) {
                console.log(error);
            }
        },
        getChildInventory_details: async (_parent, args) => {
            try {
                const data = await prisma.childInventory.findMany({
                    where: {
                        style_Code: args === null || args === void 0 ? void 0 : args.styleCode
                    },
                    include: {
                        subImageFieldOut: true
                    }
                });
                return data;
            }
            catch (error) {
                console.log(error);
            }
        },
        getCategory: async () => {
            try {
                const data = await prisma.category.findMany();
                return data;
            }
            catch (error) {
                console.log(error);
            }
        },
        getProductTypes: async () => {
            return await prisma.productTypes.findMany();
        },
        getBrand: async () => {
            return await prisma.brandname.findMany();
        },
        getRelatedProduct: async () => {
            try {
                const data = await prisma.childInventory.findMany({
                    include: {
                        Ratings: true,
                        Views: true,
                        SoldItems: true,
                        subImageFieldOut: true
                    }
                });
                const result = data.map(item => {
                    const TotalSoldItems = item.SoldItems.reduce((sum, soldItem) => sum + (soldItem === null || soldItem === void 0 ? void 0 : soldItem.Quantity), 0);
                    return Object.assign(Object.assign({}, item), { TotalSoldItems // Attach the calculated sum to the result
                     });
                });
                const resultB = result.map((item) => {
                    const Totlength = item.Ratings.length;
                    const TotalRatings = item.Ratings.reduce((sum, rating) => (sum + rating.Ratings) / Totlength, 0);
                    return Object.assign(Object.assign({}, item), { TotalRatings });
                });
                return resultB;
            }
            catch (error) {
                console.log(error);
            }
        },
        getToviewProduct: async (_parent, args) => {
            try {
                const id = args === null || args === void 0 ? void 0 : args.id;
                const data = await prisma.childInventory.findMany({
                    where: {
                        id: id === null || id === void 0 ? void 0 : id.toString()
                    },
                    include: {
                        subImageFieldOut: true,
                        Ratings: true,
                        Views: true,
                        SoldItems: true
                    }
                });
                const result = data.map(item => {
                    const TotalSoldItems = item.SoldItems.reduce((sum, soldItem) => sum + (soldItem === null || soldItem === void 0 ? void 0 : soldItem.Quantity), 0);
                    return Object.assign(Object.assign({}, item), { TotalSoldItems // Attach the calculated sum to the result
                     });
                });
                const resultB = result.map((item) => {
                    const Totlength = item.Ratings.length;
                    const TotalRatings = item.Ratings.reduce((sum, rating) => (sum + rating.Ratings) / Totlength, 0);
                    return Object.assign(Object.assign({}, item), { TotalRatings });
                });
                return resultB;
            }
            catch (error) {
                console.log(error);
            }
        },
        getLogin: async (_parent, args) => {
            var _a, _b;
            try {
                const Input = args;
                const email = (_a = Input === null || Input === void 0 ? void 0 : Input.username) === null || _a === void 0 ? void 0 : _a.toString();
                const password = (_b = Input === null || Input === void 0 ? void 0 : Input.password) === null || _b === void 0 ? void 0 : _b.toString();
                const unique = await prisma.user.findMany({
                    where: {
                        email: email,
                    }
                });
                if (unique) {
                    let encrypedpassword = unique[0].password;
                    const decrypt = await bcrypt.compare(password, encrypedpassword);
                    if (decrypt == true) {
                        const response_data = await prisma.user.findMany({
                            where: {
                                email: email,
                                password: encrypedpassword
                            }
                        });
                        const user = {
                            id: response_data[0].id,
                            userLevel: response_data[0].accountLevel,
                            iconSrc: response_data[0].image,
                            emailAddress: response_data[0].email,
                            isLoginUsingAdmin: true
                        };
                        const jwt_token = process.env.JWT_ACCESS_TOKEN_SECRET;
                        const token = jwt.sign({ user: user, }, jwt_token, { expiresIn: '24h' });
                        return {
                            jsonToken: token,
                            statusText: `Welcome!`
                        };
                    }
                    else {
                        return {
                            jsonToken: "token",
                            statusText: `Login Failed!`
                        };
                    }
                }
            }
            catch (error) {
                console.log(error);
            }
        },
        getNameofStore: async (_parent, _args) => {
            try {
                return await prisma.user.findMany({
                    where: {
                        accountLevel: "Merchant"
                    },
                    include: {
                        accountDetails: true
                    }
                });
            }
            catch (error) {
                console.log(error);
            }
        },
        getNumberOfViews: async (_parent, _args) => {
            return await prisma.numberOfViews.findMany();
        },
        getWebsitVisits: async (_parent, _args) => {
            return await prisma.websiteVisits.findMany();
        },
        getIp: async (_parent, args) => {
            const ipadd = args === null || args === void 0 ? void 0 : args.ipAddress;
            const ipData = await new Promise((resolve, reject) => {
                const url = `https://api.ip2location.io/?key=11AD281BE74BE72DABDFCBC298BBF47C&ip=${ipadd}&format=json`;
                let response = "";
                const req = https.get(url, (res) => {
                    res.on("data", (chunk) => (response = response + chunk));
                    res.on("end", () => {
                        resolve(response);
                    });
                });
                req.on("error", (err) => {
                    reject(err);
                });
            });
            return await ipData;
        },
        getComment: async (_parent, args) => {
            try {
                const messages = await prisma.comments.findMany({
                    orderBy: {
                        dateSent: 'desc'
                    }
                });
                pubsub.publish(MESSAGE_COMMENT, { messageComments: messages });
                return messages;
            }
            catch (error) {
            }
        },
        messages: async (_parent, args) => {
            try {
                const messages = await prisma.crowdMessages.findMany({
                    orderBy: {
                        dateSent: 'desc'
                    }
                });
                pubsub.publish(MESSAGE_ADDED, { messageAdded: messages });
                return messages;
            }
            catch (error) {
            }
        },
        personalMessages: async (_parent, args) => {
            try {
                const messages = await prisma.personalMessages.findMany({
                    where: {
                        OR: [
                            {
                                AND: [
                                    { Sender: args === null || args === void 0 ? void 0 : args.reciever },
                                    { Reciever: args === null || args === void 0 ? void 0 : args.emailAddress },
                                ],
                            },
                            {
                                AND: [
                                    { Sender: args === null || args === void 0 ? void 0 : args.emailAddress },
                                    { Reciever: args === null || args === void 0 ? void 0 : args.reciever },
                                ],
                            },
                        ],
                    },
                    orderBy: {
                        dateSent: 'desc',
                    },
                });
                return messages;
            }
            catch (error) {
                console.error('Error fetching personal messages:', error);
                throw new Error('Unable to fetch personal messages.');
            }
        },
        readGroupSender: async (_parent, args) => {
            try {
                const messages = await prisma.personalMessages.findMany({
                    where: {
                        Sender: args === null || args === void 0 ? void 0 : args.emailAddress
                    },
                    distinct: ["Reciever"],
                    orderBy: {
                        dateSent: 'desc'
                    }
                });
                pubsub.publish(MESSAGE_PERSONAL, { messagesPersonal: messages });
                return messages;
            }
            catch (error) {
            }
        }
    },
    Mutation: {
        upload3DModel: async (_parent, args) => {
            try {
                const { model, id } = args;
                const base64Data = model.replace(/^data:model\/\w+;base64,/, '');
                // Create a buffer from the base64 data
                const buffer = Buffer.from(base64Data, 'base64');
                const date = new Date();
                const YYYY = date.getFullYear();
                const MM = date.getMonth();
                const DD = date.getDay();
                const hh = date.getHours();
                const mm = date.getMinutes();
                const ss = date.getSeconds();
                const newName = `Model-${YYYY + "-" + MM + "-" + DD + "-" + hh + "-" + mm + -"" + ss + ".glb"}`;
                const completedName = "https://tsbriguuaznlvwbnylop.supabase.co/storage/v1/object/public/legitemfiles/Models/" + newName;
                const uploadRes = await uploadFileToSupabaseGLB(newName, buffer, 'legitemfiles', 'Models');
                const data = await prisma.childInventory.update({
                    where: {
                        id: id === null || id === void 0 ? void 0 : id.toString()
                    },
                    data: {
                        model: completedName
                    }
                });
                if (data) {
                    return {
                        "statusText": "Success"
                    };
                }
            }
            catch (error) {
                console.log(error);
            }
        },
        uploadCategoryImage: async (_parent, args) => {
            try {
                const { image, id } = args;
                const base64Data = image.replace(/^data:image\/\w+;base64,/, '');
                // Create a buffer from the base64 data
                const buffer = Buffer.from(base64Data, 'base64');
                const date = new Date();
                const YYYY = date.getFullYear();
                const MM = date.getMonth();
                const DD = date.getDay();
                const hh = date.getHours();
                const mm = date.getMinutes();
                const ss = date.getSeconds();
                const newName = `Category-${YYYY + "-" + MM + "-" + DD + "-" + hh + "-" + mm + -"" + ss + ".webp"}`;
                const completedName = "https://tsbriguuaznlvwbnylop.supabase.co/storage/v1/object/public/legitemfiles/CategoryImages/" + newName;
                const uploadRes = await uploadFileToSupabase(newName, buffer, 'legitemfiles', 'CategoryImages');
                const data = await prisma.category.update({
                    where: {
                        id: id === null || id === void 0 ? void 0 : id.toString()
                    },
                    data: {
                        image: completedName
                    }
                });
                if (data) {
                    return {
                        "statusText": "Successful"
                    };
                }
            }
            catch (error) {
            }
        },
        insertBrand: async (_parent, args) => {
            try {
                const Input = args.BrandnameInput;
                const data = await prisma.brandname.create({
                    data: {
                        ProductType: Input === null || Input === void 0 ? void 0 : Input.ProductType,
                        Name: Input === null || Input === void 0 ? void 0 : Input.Name
                    }
                });
                if (data) {
                    return {
                        "statusText": "Successful"
                    };
                }
            }
            catch (error) {
                console.log(error);
            }
        },
        insertProductTypes: async (_parent, args) => {
            try {
                const Input = args.ProductTypesInput;
                const data = await prisma.productTypes.create({
                    data: {
                        Category: Input === null || Input === void 0 ? void 0 : Input.Category,
                        Name: Input === null || Input === void 0 ? void 0 : Input.Name
                    }
                });
                if (data) {
                    return {
                        "statusText": "Successful"
                    };
                }
            }
            catch (error) {
                console.log(error);
            }
        },
        insertCategory: async (_parent, args) => {
            try {
                const { Name, status, icon, image } = args.CategoryInput;
                const base64Data = image.replace(/^data:image\/\w+;base64,/, '');
                // Create a buffer from the base64 data
                const buffer = Buffer.from(base64Data, 'base64');
                const date = new Date();
                const YYYY = date.getFullYear();
                const MM = date.getMonth();
                const DD = date.getDay();
                const hh = date.getHours();
                const mm = date.getMinutes();
                const ss = date.getSeconds();
                const newName = `Category-${YYYY + "-" + MM + "-" + DD + "-" + hh + "-" + mm + -"" + ss + ".webp"}`;
                const completedName = "https://tsbriguuaznlvwbnylop.supabase.co/storage/v1/object/public/legitemfiles/CategoryImages/" + newName;
                const data = {
                    Name: Name,
                    status: status,
                    icon: icon,
                    image: completedName
                };
                const data_2 = await uploadFileToSupabase(newName, buffer, 'legitemfiles', 'CategoryImages');
                const result = await prisma.category.create({
                    data
                });
                if (result) {
                    return {
                        "statusText": "Successful!"
                    };
                }
            }
            catch (error) {
                console.log(error);
            }
        },
        insertLikes: async (_parent, args) => {
            try {
                const { productCode, accountEmail } = args.LikesParamInput;
                const result = await prisma.likes.create({
                    data: {
                        productCode: productCode,
                        accountEmail: accountEmail
                    }
                });
                if (result) {
                    return {
                        "statusText": "Successful!"
                    };
                }
            }
            catch (error) {
                console.log(error);
            }
        },
        insertProductFeedBacks: async (_parent, args) => {
            try {
                const ProductFeedBacksInput = args.ProductFeedBacksInput;
                ProductFeedBacksInput.map(async (item) => {
                    let completedName;
                    const file = item.Attachment;
                    if (file !== null || file !== "") {
                        const base64Data = file.replace(/^data:image\/\w+;base64,/, '');
                        // Create a buffer from the base64 data
                        const buffer = Buffer.from(base64Data, 'base64');
                        const date = new Date();
                        const YYYY = date.getFullYear();
                        const MM = date.getMonth();
                        const DD = date.getDay();
                        const hh = date.getHours();
                        const mm = date.getMinutes();
                        const ss = date.getSeconds();
                        const newName = `Attachement-${YYYY + "-" + MM + "-" + DD + "-" + hh + "-" + mm + -"" + ss + ".webp"}`;
                        completedName = "https://tsbriguuaznlvwbnylop.supabase.co/storage/v1/object/public/legitemfiles/FeedBackAttachement/" + newName;
                        const uploadRes = await uploadFileToSupabase(newName, buffer, 'legitemfiles', 'FeedBackAttachement');
                    }
                    else {
                        completedName = "";
                    }
                    const result = await prisma.productFeedBacks.create({
                        data: {
                            productCode: item.productCode,
                            OrderNo: item.OrderNo,
                            Ratings: item.Ratings,
                            Attachment: completedName,
                            Comment: item.Comment,
                            By: item.By
                        }
                    });
                    return result;
                });
                return {
                    "statusText": "Thanks for yor feedback!"
                };
            }
            catch (error) {
            }
        },
        insertPrivacy: async (_parent, args) => {
            try {
                const content = args.content;
                const newPrivacy = await prisma.privacy.create({
                    data: {
                        content: content
                    }
                });
                if (newPrivacy) {
                    return {
                        "statusText": "Privacy Policy Added Successfully"
                    };
                }
            }
            catch (error) {
                console.log(error);
            }
        },
        insertDisclaimer: async (_parent, args) => {
            try {
                const content = args.content;
                const newdisclaimer = await prisma.disclaimer.create({
                    data: {
                        content: content
                    }
                });
                if (newdisclaimer) {
                    return {
                        "statusText": "Disclaimer Added Successfully"
                    };
                }
            }
            catch (error) {
                console.log(error);
            }
        },
        insertAbout: async (_parent, args) => {
            try {
                const content = args.content;
                const newinsertAbout = await prisma.about.create({
                    data: {
                        content: content
                    }
                });
                if (newinsertAbout) {
                    return {
                        "statusText": "About Added Successfully"
                    };
                }
            }
            catch (error) {
                console.log(error);
            }
        },
        insertShippingDetails: async (_parent, args) => {
            try {
                const { shippingDetailsInput } = args;
                const result = await prisma.accountDetails.create({
                    data: shippingDetailsInput
                });
                if (result) {
                    return {
                        "statusText": "Successfully Inserted!"
                    };
                }
            }
            catch (error) {
                console.log(error);
            }
        },
        deleteShippingDetails: async (_parent, args) => {
            try {
                const id = args.id;
                const result = await prisma.accountDetails.delete({
                    where: {
                        id: id
                    }
                });
                if (result) {
                    return {
                        "statusText": "Successfully Deleted!"
                    };
                }
            }
            catch (error) {
                console.log(error);
            }
        },
        deletePrivacy: async (_parent, args) => {
            try {
                const id = args.id;
                const newPrivacy = await prisma.privacy.delete({
                    where: {
                        id: id
                    }
                });
                if (newPrivacy) {
                    return {
                        "statusText": "Privacy Policy Deleted Successfully"
                    };
                }
            }
            catch (error) {
                console.log(error);
            }
        },
        deleteDisclaimer: async (_parent, args) => {
            try {
                const id = args.id;
                const newdisclaimer = await prisma.disclaimer.delete({
                    where: {
                        id: id
                    }
                });
                if (newdisclaimer) {
                    return {
                        "statusText": "Disclaimer Deleted Successfully"
                    };
                }
            }
            catch (error) {
                console.log(error);
            }
        },
        deleteAbout: async (_parent, args) => {
            try {
                const id = args.id;
                const newAbout = await prisma.about.delete({
                    where: {
                        id: id
                    }
                });
                if (newAbout) {
                    return {
                        "statusText": "About Deleted Successfully"
                    };
                }
            }
            catch (error) {
                console.log(error);
            }
        },
        setActiveUsers: async (_parent, args) => {
            try {
                let result = await prisma.user.findUnique({
                    where: {
                        email: args.emailAddress,
                    }
                });
                const result2 = await prisma.accountDetails.findMany({
                    where: {
                        accountEmail: result.email,
                        defaultAddress: true
                    }
                });
                const data = {
                    "accountEmail": result.email,
                    "fullname": result2[0].fullname
                };
                Active.push(data);
                pubsub.publish(ACTIVE_USERS, { ActiveUserList: Active });
                return Active;
            }
            catch (error) {
            }
        },
        updateAccountDetails: async (_parent, args) => {
            try {
                let result = 'Success';
                const { updateAccountDetailsInput } = args;
                const filtered = await prisma.accountDetails.findMany({
                    where: {
                        accountEmail: updateAccountDetailsInput.email,
                        defaultAddress: true
                    }
                });
                if (filtered.length == 0) {
                    result = 'Default Address Not Found';
                }
                else {
                    await prisma.user.update({
                        where: {
                            email: updateAccountDetailsInput.email
                        },
                        data: {
                            accountLevel: updateAccountDetailsInput.accountLevel,
                            agentIdentity: updateAccountDetailsInput.agentIdentity,
                            email: updateAccountDetailsInput.email,
                            loginAttemp: updateAccountDetailsInput.loginAttemp,
                            macAddress: updateAccountDetailsInput.macAddress,
                            nameOfStore: updateAccountDetailsInput.nameOfStore
                        }
                    });
                    await prisma.accountDetails.updateMany({
                        where: {
                            accountEmail: updateAccountDetailsInput.email,
                            defaultAddress: true
                        },
                        data: {
                            fullname: updateAccountDetailsInput.accountDetails.fullname,
                            storeName: updateAccountDetailsInput.accountDetails.storeName,
                            contactNo: updateAccountDetailsInput.accountDetails.contactNo,
                            Address: updateAccountDetailsInput.accountDetails.Address
                        }
                    });
                }
                return {
                    "statusText": result
                };
            }
            catch (error) {
            }
        },
        updateOrderStatusRecieved: async (_parent, args) => {
            try {
                const { OrderstatusParameter } = args;
                const { OrderNo, agentEmail } = OrderstatusParameter;
                const result = await prisma.orderHistory.updateMany({
                    where: {
                        OrderNo: OrderNo,
                        agentEmail: agentEmail
                    },
                    data: {
                        OrderStatus: 'Recieved',
                        StatusText: 'Your order has received and on process'
                    }
                });
                const OrderMessage = await prisma.orderHistory.findMany({
                    where: {
                        OrderNo: OrderNo
                    }
                });
                pubsub.publish(MESSAGE_TO_ORDER, { messageToOrder: OrderMessage });
                if (result) {
                    return {
                        'statusText': 'Successful!'
                    };
                }
            }
            catch (error) {
            }
        },
        updateOrderStatusPacked: async (_parent, args) => {
            try {
                const { OrderstatusParameter } = args;
                const { OrderNo, agentEmail } = OrderstatusParameter;
                const result = await prisma.orderHistory.updateMany({
                    where: {
                        OrderNo: OrderNo,
                        agentEmail: agentEmail
                    },
                    data: {
                        OrderStatus: 'Packed',
                        StatusText: 'Your order is Packed!'
                    }
                });
                const OrderMessage = await prisma.orderHistory.findMany({
                    where: {
                        OrderNo: OrderNo
                    }
                });
                pubsub.publish(MESSAGE_TO_ORDER, { messageToOrder: OrderMessage });
                if (result) {
                    return {
                        'statusText': 'Successful!'
                    };
                }
            }
            catch (error) {
            }
        },
        updateOrderStatusLogistic: async (_parent, args) => {
            try {
                const { OrderstatusParameter } = args;
                const { OrderNo, agentEmail } = OrderstatusParameter;
                const result = await prisma.orderHistory.updateMany({
                    where: {
                        OrderNo: OrderNo,
                        agentEmail: agentEmail
                    },
                    data: {
                        OrderStatus: 'Logistic',
                        StatusText: 'Your order has brought on logistic partner',
                        TrackingNo: generateTrackingNumber()
                    }
                });
                const OrderMessage = await prisma.orderHistory.findMany({
                    where: {
                        OrderNo: OrderNo
                    }
                });
                pubsub.publish(MESSAGE_TO_ORDER, { messageToOrder: OrderMessage });
                if (result) {
                    return {
                        'statusText': 'Successful!'
                    };
                }
            }
            catch (error) {
            }
        },
        updateOrderStatusDelivery: async (_parent, args) => {
            try {
                const { OrderstatusParameter } = args;
                const { OrderNo, agentEmail } = OrderstatusParameter;
                const result = await prisma.orderHistory.updateMany({
                    where: {
                        OrderNo: OrderNo,
                        agentEmail: agentEmail
                    },
                    data: {
                        OrderStatus: 'Delivery',
                        StatusText: 'Your order is out for delivery'
                    }
                });
                const OrderMessage = await prisma.orderHistory.findMany({
                    where: {
                        OrderNo: OrderNo
                    }
                });
                pubsub.publish(MESSAGE_TO_ORDER, { messageToOrder: OrderMessage });
                if (result) {
                    return {
                        'statusText': 'Successful!'
                    };
                }
            }
            catch (error) {
            }
        },
        updateProductFeedBackStatus: async (_parent, args) => {
            try {
                const { ProductFeedBacksStatusParameter } = args;
                ProductFeedBacksStatusParameter;
                ProductFeedBacksStatusParameter.map(async (item) => {
                    const { OrderNo, agentEmail } = item;
                    const result = await prisma.orderHistory.updateMany({
                        where: {
                            OrderNo: OrderNo,
                            agentEmail: agentEmail
                        },
                        data: {
                            OrderStatus: 'FeedBack',
                            StatusText: 'Thank you for the feedback!'
                        }
                    });
                });
                return {
                    'statusText': 'Successful!'
                };
            }
            catch (error) {
                console.log(error);
            }
        },
        updateOrderStatusDelivered: async (_parent, args) => {
            try {
                const { OrderstatusParameter } = args;
                const { OrderNo, agentEmail } = OrderstatusParameter;
                const result = await prisma.orderHistory.updateMany({
                    where: {
                        OrderNo: OrderNo,
                        agentEmail: agentEmail
                    },
                    data: {
                        OrderStatus: 'Delivered',
                        StatusText: 'Your Order Successfully Delivered'
                    }
                });
                const OrderMessage = await prisma.orderHistory.findMany({
                    where: {
                        OrderNo: OrderNo
                    }
                });
                pubsub.publish(MESSAGE_TO_ORDER, { messageToOrder: OrderMessage });
                if (result) {
                    return {
                        'statusText': 'Successful!'
                    };
                }
            }
            catch (error) {
            }
        },
        insertOrder: async (_parent, args) => {
            const parameters = args.OrderHistoryInput;
            // const tracking = generateTrackingNumber();
            const orderno = generateOrderNumber();
            // Ensure all the promises are resolved before returning the result
            const result = await Promise.all(parameters.map(async (item) => {
                return await prisma.orderHistory.create({
                    data: {
                        productCode: item.productCode,
                        Image: item.Image,
                        Size: item.Size,
                        Color: item.Color,
                        emailAddress: item.emailAddress,
                        TrackingNo: null,
                        OrderNo: orderno,
                        Quantity: item.Quantity,
                        Price: item.Price,
                        Address: item.Address,
                        Contact: item.Contact,
                        agentEmail: item.agentEmail,
                        paymentMethod: item.paymentMethod
                    },
                });
            }));
            const OrderMessage = await prisma.orderHistory.findMany({
                where: {
                    OrderNo: orderno
                }
            });
            pubsub.publish(MESSAGE_TO_ORDER, { messageToOrder: OrderMessage });
            if (result.length > 0) {
                return {
                    statusText: "Success",
                };
            }
        },
        insertNumberOfVisit: async (_parent, args) => {
            const date_today = new Date();
            const yy = date_today.getFullYear();
            const mm = date_today.getMonth() + 1;
            const dd = date_today.getDay();
            const dateData = `${yy}/${mm}/${dd}`;
            const data = await prisma.websiteVisits.findMany({
                where: {
                    IpAddress: args.IpAddress,
                    dateVisited: dateData
                }
            });
            if (data.length < 1) {
                await prisma.websiteVisits.create({
                    data: {
                        IpAddress: args.IpAddress,
                        Country: args.Country,
                        dateVisited: dateData
                    }
                });
            }
        },
        insertNumberOfViews: async (_parent, args) => {
            try {
                const date_today = new Date();
                const yy = date_today.getFullYear();
                const mm = date_today.getMonth() + 1;
                const dd = date_today.getDay();
                const dateData = `${yy}/${mm}/${dd}`;
                const data = await prisma.numberOfViews.findMany({
                    where: {
                        productCode: args.productCode,
                        dateVisited: dateData
                    }
                });
                if (data.length < 1) {
                    await prisma.numberOfViews.create({
                        data: {
                            count: args.count,
                            productCode: args.productCode,
                            emailAddress: args.emailAddress,
                            IpAddress: args.IpAddress,
                            Country: args.Country,
                            dateVisited: dateData
                        }
                    });
                    return {
                        "statusText": "Successfully Viewed Counted"
                    };
                }
                else {
                    return {
                        "statusText": "Already Viewed!"
                    };
                }
            }
            catch (error) {
            }
        },
        insertUser: async (_parent, args) => {
            try {
                //*****************************************************//
                var date = new Date();
                var YYYY = date.getFullYear();
                var MMMM = date.getMonth();
                var DDDD = date.getDay();
                var hh = date.getHours();
                var mm = date.getMinutes();
                var ss = date.getSeconds();
                var emailAddress = args.emailAddress;
                var passWord = args.password;
                let agentIdentity = args.agentIdentity;
                const account_acct_code = "Acct" + "-" + YYYY + "-" + MMMM + "-" + DDDD + "-" + hh + "-" + mm + "-" + ss;
                const salt = bcrypt.genSaltSync(10);
                const hash = bcrypt.hashSync(passWord, salt);
                const d = new Date();
                var init = 'AC';
                var mm = d.getMinutes();
                var ss = d.getSeconds();
                //*****************************************************//
                const filter = await prisma.user.findMany({
                    where: {
                        email: emailAddress
                    }
                });
                if (filter.length > 0) {
                    return {
                        statusText: "This Email is already taken"
                    };
                }
                else {
                    await prisma.user.create({
                        data: {
                            email: emailAddress,
                            accountCode: account_acct_code,
                            password: hash,
                            accountLevel: "Encoder",
                            agentIdentity: agentIdentity,
                            accountDetails: {
                                create: {
                                    accountEmail: emailAddress,
                                    defaultAddress: true
                                }
                            }
                        }
                    });
                    return {
                        statusText: "Successfully"
                    };
                }
            }
            catch (error) {
                console.log(error);
            }
        },
        insertInventory: async (_parent, args) => {
            var date = new Date();
            var YYYY = date.getFullYear();
            var MMMM = date.getMonth();
            var DDDD = date.getDay();
            var hh = date.getHours();
            var mm = date.getMinutes();
            var ss = date.getSeconds();
            var emailAddress = args.emailAddress;
            const stl = "Style_" + YYYY + "-" + MMMM + "-" + DDDD + "-" + hh + "-" + mm + "-" + ss;
            const prcode = "PrCode_" + YYYY + MMMM + DDDD + hh + mm + ss;
            await prisma.inventory.create({
                data: {
                    styleCode: stl,
                    productType: args.productType,
                    category: args.category,
                    name: args.productName,
                    status: "Active",
                    agentEmail: args.emailAddress,
                    brandname: args.brandname,
                    collectionItem: ""
                }
            });
            await prisma.childInventory.create({
                data: {
                    style_Code: stl,
                    productCode: prcode,
                    productType: args.productType,
                    brandname: args.brandname,
                    category: args.category,
                    price: 0,
                    stock: 0,
                    status: "Active",
                    creator: emailAddress,
                    editor: emailAddress,
                    agentEmail: emailAddress,
                    name: args.productName
                }
            });
            return {
                statusText: "Successfully!"
            };
        },
        insertChildInventory: async (_parent, args) => {
            var date = new Date();
            var YYYY = date.getFullYear();
            var MMMM = date.getMonth();
            var DDDD = date.getDay();
            var hh = date.getHours();
            var mm = date.getMinutes();
            var ss = date.getSeconds();
            var emailAddress = args.emailAddress;
            var styleCode = args.styleCode;
            const prcode = "PrCode_" + YYYY + MMMM + DDDD + hh + mm + ss;
            const data = await prisma.childInventory.create({
                data: {
                    agentEmail: emailAddress,
                    stock: parseFloat(args.productStock),
                    price: parseFloat(args.productPrice),
                    color: args.productColor,
                    size: args.productSize,
                    category: args.category,
                    productType: args.productType,
                    brandname: args.brandname,
                    name: args.productName,
                    productDescription: args.productDescription,
                    style_Code: styleCode,
                    productCode: prcode,
                    status: "Active",
                    creator: emailAddress,
                    editor: emailAddress,
                }
            });
            if (data) {
                return {
                    "statusText": "Successfully Inserted!"
                };
            }
        },
        createLinkToChangePassword: async (_parent, args) => {
            try {
                let result = 'Success';
                const { path, emailAddress } = args;
                const userData = await prisma.user.findMany({
                    where: {
                        email: emailAddress
                    }
                });
                const EmailAddress = emailAddress;
                const link = `${path}ForgotPassword/?id=${encode(EmailAddress)}`;
                if (userData.length > 0) {
                    result = "Check your Email";
                }
                else {
                    result = "Email Address Does Not Exist";
                }
                await createLinkURL(emailAddress, link);
                return {
                    statusText: result,
                };
            }
            catch (error) {
                console.error('Error creating password reset link:', error);
                return {
                    status: 'error',
                    statusText: 'An error occurred while processing your request.',
                };
            }
        },
        contactUs: async (_parent, args) => {
            const result = await ContactUs(args.messagebody);
            return {
                "statusText": "Successfully"
            };
        },
        updatePassword: async (_parent, args) => {
            try {
                const password = args.password;
                const emailAddress = args.emailAddress;
                const salt = bcrypt.genSaltSync(10);
                const hash = bcrypt.hashSync(password, salt);
                const update = await prisma.user.update({
                    where: {
                        email: emailAddress
                    },
                    data: {
                        password: hash
                    }
                });
                return {
                    "statusText": "Successfully"
                };
            }
            catch (error) {
            }
        },
        updateParentInventory: async (_parent, args) => {
            var date = new Date();
            const result = await prisma.inventory.update({
                where: {
                    id: args.productID
                },
                data: {
                    productType: args.productType,
                    category: args.category,
                    name: args.productName,
                    status: args.status,
                    dateUpdated: date,
                    brandname: args.brandname
                }
            });
            await prisma.childInventory.updateMany({
                where: {
                    style_Code: result.styleCode
                },
                data: {
                    name: args.productName,
                    productType: args.productType,
                    brandname: args.brandname
                }
            });
            return {
                "statusText": "Success!"
            };
        },
        updateChildInventory: async (_parent, args) => {
            var date = new Date();
            await prisma.childInventory.update({
                where: {
                    id: args.id
                },
                data: {
                    color: args.productColor,
                    size: args.productSize,
                    price: parseFloat(args.productPrice),
                    stock: parseInt(args.productStock),
                    productDescription: args.productDescription,
                    status: args.productStatus,
                    editor: args.Email,
                    dateUpdated: date
                }
            });
            return {
                "statusText": "Successfuly Save!",
                "jsonToken": "String"
            };
        },
        saveCropImage: async (_parent, args) => {
            const id = args.id;
            const file = args.file;
            const base64Data = file.replace(/^data:image\/\w+;base64,/, '');
            // Create a buffer from the base64 data
            const buffer = Buffer.from(base64Data, 'base64');
            const date = new Date();
            const YYYY = date.getFullYear();
            const MM = date.getMonth();
            const DD = date.getDay();
            const hh = date.getHours();
            const mm = date.getMinutes();
            const ss = date.getSeconds();
            const newName = `Product-${YYYY + "-" + MM + "-" + DD + "-" + hh + "-" + mm + -"" + ss + ".webp"}`;
            const uploadRes = await uploadFileToSupabase(newName, buffer, 'legitemfiles', 'ProductImages');
            const completedName = "https://tsbriguuaznlvwbnylop.supabase.co/storage/v1/object/public/legitemfiles/ProductImages/" + newName;
            const saved = await prisma.childInventory.update({
                where: {
                    id: id
                },
                data: {
                    thumbnail: completedName
                }
            });
            const result = await prisma.inv_subImage.create({
                data: {
                    ImagePath: completedName,
                    subImageRelationChild: id
                }
            });
            return {
                "statusText": "Image saved successfully"
            };
        },
        postMessage: async (_parent, args) => {
            try {
                const Live = args.Live;
                const Video = args.Video;
                const newMessage = await prisma.crowdMessages.create({
                    data: {
                        Messages: args.Message,
                        Sender: args.Sender
                    }
                });
                const messages = {
                    id: newMessage.id,
                    Messages: newMessage.Messages,
                    Sender: newMessage.Sender,
                    Live: Live,
                    Video: Video,
                    dateSent: newMessage.dateSent
                };
                pubsub.publish(MESSAGE_ADDED, { messageAdded: messages });
                return newMessage;
            }
            catch (error) {
                console.error("Error creating message:", error);
            }
        },
        live: async (_parent, args) => {
            try {
                const newMessage = await prisma.crowdMessages.create({
                    data: {
                        Messages: args.Message,
                        Sender: args.Sender
                    }
                });
                const messages = {
                    id: newMessage.id,
                    Messages: args.Messages,
                    Sender: args.Sender,
                    Live: args.Live,
                    Video: args.Video,
                    dateSent: newMessage.dateSent
                };
                pubsub.publish(MESSAGE_ADDED, { messageAdded: messages });
                return messages;
            }
            catch (error) {
                console.error("Error creating message:", error);
            }
        },
        postComment: async (_parent, args) => {
            try {
                const newMessage = await prisma.comments.create({
                    data: {
                        Messages: args.Message,
                        Sender: args.Sender,
                        CrowID: args.CrowdID
                    }
                });
                pubsub.publish(MESSAGE_COMMENT, { messageComments: newMessage });
                return newMessage;
            }
            catch (error) {
                console.error("Error creating message:", error);
            }
        },
        postPersonalMessage: async (_parent, args) => {
            try {
                const newMessage = await prisma.personalMessages.create({
                    data: {
                        Messages: args.Message,
                        Sender: args.Sender,
                        Reciever: args.Reciever
                    }
                });
                pubsub.publish(MESSAGE_PERSONAL, { messagesPersonal: [newMessage] });
                return newMessage;
            }
            catch (error) {
                console.error("Error creating message:", error);
            }
        },
        ReadLogin: async (_parent, args) => {
            try {
                const unique = await prisma.user.findUnique({
                    where: {
                        email: args.username,
                    }
                });
                if (unique) {
                    let encrypedpassword = unique.password;
                    const decrypt = await bcrypt.compare(args.password, encrypedpassword);
                    if (decrypt == true) {
                        const response_data = await prisma.user.findMany({
                            where: {
                                email: args.username,
                                password: encrypedpassword
                            }
                        });
                        const user = {
                            id: response_data[0].id,
                            userLevel: response_data[0].accountLevel,
                            iconSrc: response_data[0].image,
                            emailAddress: response_data[0].email,
                            isLoginUsingAdmin: true
                        };
                        const jwt_token = process.env.JWT_ACCESS_TOKEN_SECRET;
                        const token = jwt.sign({ user: user, }, jwt_token, { expiresIn: '24h' });
                        return {
                            jsonToken: token,
                            statusText: `Welcome!`
                        };
                    }
                    else {
                        return {
                            jsonToken: "token",
                            statusText: `Login Failed!`
                        };
                    }
                }
            }
            catch (error) {
                console.log(error);
            }
        },
        insertSignUp: async (_parent, args) => {
            try {
                const date = new Date();
                const YYYY = date.getFullYear();
                const MMMM = date.getMonth() + 1; // Month is zero-based, so add 1
                const DDDD = date.getDate(); // Use `getDate` instead of `getDay` to get the day of the month
                const hh = date.getHours();
                const mm = date.getMinutes();
                const ss = date.getSeconds();
                const generatePassword = (password) => {
                    const salt = bcrypt.genSaltSync(10);
                    const hash = bcrypt.hashSync(password, salt);
                    return hash;
                };
                const accountCode = `Acct-${YYYY}-${MMMM}-${DDDD}-${hh}-${mm}-${ss}`;
                const signUpParameters = (args === null || args === void 0 ? void 0 : args.SignUpParameters) || [];
                let result = "Successfully Saved!";
                for (const item of signUpParameters) {
                    const checkExistence = await prisma.user.findMany({
                        where: {
                            email: item.emailAddress,
                        },
                    });
                    if (checkExistence.length > 0) {
                        result = "Email already exists";
                    }
                    else {
                        await sendMail(item.emailAddress, item.fullname);
                        await prisma.user.create({
                            data: {
                                email: item.emailAddress,
                                password: generatePassword(item.PassWord),
                                accountCode: accountCode,
                                accountLevel: 'User',
                                agentIdentity: item.emailAddress,
                                accountDetails: {
                                    create: {
                                        fullname: item.fullname,
                                        accountEmail: item.emailAddress,
                                        contactNo: item.contactNo,
                                        defaultAddress: true
                                    },
                                },
                            },
                        });
                        result = "Successfully Saved!";
                    }
                }
                return {
                    statusText: result,
                };
            }
            catch (error) {
                console.error("Error during sign-up:", error);
                return {
                    statusText: "Error during sign-up.",
                };
            }
        },
        insertNews: async (_parent, args) => {
            const { NewsInput } = args;
            const newNews = await prisma.news.create({
                data: {
                    title: NewsInput.title,
                    thumbnail: NewsInput.thumbnail,
                    summary: NewsInput.summary,
                    postedBy: NewsInput.postedBy
                }
            });
            const messages = await prisma.news.findMany();
            pubsub.publish(MESSAGE_NEWS, { messageNews: [newNews] });
            return {
                "statusText": "Successfull!"
            };
        },
        deleteChildInventory: async (_parent, args) => {
            try {
                const deleteItem = await prisma.childInventory.delete({
                    where: {
                        id: args.id
                    }
                });
                if (deleteItem) {
                    return {
                        "statusText": "Successfull deleted!"
                    };
                }
            }
            catch (error) {
                console.error("Error deleting item:", error);
            }
        },
        updateNews: async (_parent, args) => {
            try {
                const { title, thumbnail, summary, postedBy } = args.NewsInput;
                await prisma.news.update({
                    where: {
                        id: args.param
                    },
                    data: {
                        title: title,
                        thumbnail: thumbnail,
                        summary: summary,
                        postedBy: postedBy
                    }
                });
                return {
                    "statusText": "Successfully Updated!"
                };
            }
            catch (error) {
                console.log(error);
            }
        },
        deleteNews: async (_parent, args) => {
            try {
                const { param } = args;
                const result = await prisma.news.delete({
                    where: {
                        id: param
                    }
                });
                if (result) {
                    return {
                        "statusText": "Successfully deleted!"
                    };
                }
            }
            catch (error) {
                console.log(error);
            }
        },
        updateDefaultAddress: async (_parent, args) => {
            try {
                const { id, accountEmail } = args;
                // First, update all addresses to have defaultAddress as false
                const initialResult = await prisma.accountDetails.updateMany({
                    where: { accountEmail },
                    data: { defaultAddress: false },
                });
                if (initialResult.count > 0) { // Ensure that at least one record was updated
                    // Then, update the specific address to be the default one
                    const finalResult = await prisma.accountDetails.update({
                        where: { id },
                        data: { defaultAddress: true },
                    });
                    if (finalResult) {
                        return {
                            statusText: "Successfully Updated!",
                        };
                    }
                }
                else {
                    return {
                        statusText: "No addresses were found to update.",
                    };
                }
            }
            catch (error) {
                console.log(error);
                return {
                    statusText: "Failed to update address.",
                };
            }
        },
        startCall: async (_parent, args) => {
            // Logic to handle starting a call
            const { from, to } = args.callInput;
            const call = { from, to };
            // Publish the call received event
            pubsub.publish(CALL_RECEIVED, { callReceived: call });
            return call;
        },
    },
    Subscription: {
        messageAdded: {
            subscribe: () => pubsub.asyncIterator([MESSAGE_ADDED]),
        },
        messageComments: {
            subscribe: () => pubsub.asyncIterator([MESSAGE_COMMENT]),
        },
        messageToOrder: {
            subscribe: () => pubsub.asyncIterator([MESSAGE_TO_ORDER])
        },
        messageNews: {
            subscribe: () => pubsub.asyncIterator([MESSAGE_NEWS])
        },
        ActiveUserList: {
            subscribe: () => pubsub.asyncIterator([ACTIVE_USERS])
        },
        messagesPersonal: {
            subscribe: () => pubsub.asyncIterator([MESSAGE_PERSONAL]),
        },
        callReceived: {
            subscribe: () => pubsub.asyncIterator([CALL_RECEIVED]),
        },
        iceCandidateReceived: {
            subscribe: () => pubsub.asyncIterator([ICE_CANDIDATE_RECEIVED]),
        },
    },
};
