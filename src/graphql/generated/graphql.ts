/* eslint-disable */
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  Buffer: { input: any; output: any; }
  Date: { input: any; output: any; }
  Upload: { input: any; output: any; }
};

export type About = {
  __typename?: 'About';
  content?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
};

export type AccountDetails = {
  __typename?: 'AccountDetails';
  Address?: Maybe<Scalars['String']['output']>;
  accountEmail?: Maybe<Scalars['String']['output']>;
  contactNo?: Maybe<Scalars['String']['output']>;
  defaultAddress?: Maybe<Scalars['Boolean']['output']>;
  fullname?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  storeName?: Maybe<Scalars['String']['output']>;
  userId?: Maybe<Scalars['String']['output']>;
};

export type ActivatedUsers = {
  __typename?: 'ActivatedUsers';
  accountEmail?: Maybe<Scalars['String']['output']>;
  fullname?: Maybe<Scalars['String']['output']>;
};

export type ActiveUsers = {
  __typename?: 'ActiveUsers';
  accountEmail?: Maybe<Scalars['String']['output']>;
  fullname?: Maybe<Scalars['String']['output']>;
};

export type Brandname = {
  __typename?: 'Brandname';
  Name?: Maybe<Scalars['String']['output']>;
  ProductType?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
};

export type BrandnameInput = {
  Name?: InputMaybe<Scalars['String']['input']>;
  ProductType?: InputMaybe<Scalars['String']['input']>;
};

export type Call = {
  __typename?: 'Call';
  from: User;
  to: User;
};

export type CallInput = {
  from: Scalars['ID']['input'];
  to: Scalars['ID']['input'];
};

export type Category = {
  __typename?: 'Category';
  Name?: Maybe<Scalars['String']['output']>;
  icon?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  image?: Maybe<Scalars['String']['output']>;
  status?: Maybe<Scalars['String']['output']>;
};

export type CategoryInput = {
  Name?: InputMaybe<Scalars['String']['input']>;
  icon?: InputMaybe<Scalars['String']['input']>;
  image?: InputMaybe<Scalars['Upload']['input']>;
  status?: InputMaybe<Scalars['String']['input']>;
};

export type ChildInventory = {
  __typename?: 'ChildInventory';
  Likes?: Maybe<Array<Maybe<Likes>>>;
  Ratings?: Maybe<Array<Maybe<ProductFeedBacks>>>;
  SoldItems?: Maybe<Array<Maybe<OrderHistory_Total_Quantity>>>;
  TotalRatings?: Maybe<Scalars['Float']['output']>;
  TotalSoldItems?: Maybe<Scalars['Int']['output']>;
  Views?: Maybe<Array<Maybe<NumberOfViews>>>;
  agentEmail?: Maybe<Scalars['String']['output']>;
  brandname?: Maybe<Scalars['String']['output']>;
  category?: Maybe<Scalars['String']['output']>;
  color?: Maybe<Scalars['String']['output']>;
  creator?: Maybe<Scalars['String']['output']>;
  dateCreated?: Maybe<Scalars['String']['output']>;
  dateUpdated?: Maybe<Scalars['String']['output']>;
  discount?: Maybe<Scalars['Float']['output']>;
  editor?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  imageReferences?: Maybe<Scalars['String']['output']>;
  model?: Maybe<Scalars['String']['output']>;
  name?: Maybe<Scalars['String']['output']>;
  parentId?: Maybe<Scalars['String']['output']>;
  paymentMethod?: Maybe<Scalars['String']['output']>;
  price?: Maybe<Scalars['String']['output']>;
  productCode?: Maybe<Scalars['String']['output']>;
  productDescription?: Maybe<Scalars['String']['output']>;
  productType?: Maybe<Scalars['String']['output']>;
  size?: Maybe<Scalars['String']['output']>;
  status?: Maybe<Scalars['String']['output']>;
  stock?: Maybe<Scalars['String']['output']>;
  style_Code?: Maybe<Scalars['String']['output']>;
  subImageFieldOut?: Maybe<Array<Maybe<Inv_SubImage>>>;
  thumbnail?: Maybe<Scalars['String']['output']>;
};

export type CrowdMessages = {
  __typename?: 'CrowdMessages';
  Messages?: Maybe<Scalars['String']['output']>;
  Sender?: Maybe<Scalars['String']['output']>;
  dateSent?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
};

export type Disclaimer = {
  __typename?: 'Disclaimer';
  content?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
};

export type Faq = {
  __typename?: 'FAQ';
  answer?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  question?: Maybe<Scalars['String']['output']>;
};

export type GroupedOrders = {
  __typename?: 'GroupedOrders';
  Address?: Maybe<Scalars['String']['output']>;
  Contact?: Maybe<Scalars['String']['output']>;
  OrderHistory?: Maybe<Array<Maybe<OrderHistory>>>;
  OrderNo?: Maybe<Scalars['String']['output']>;
  OrderStatus?: Maybe<Scalars['String']['output']>;
  StatusText?: Maybe<Scalars['String']['output']>;
};

export type IceCandidate = {
  __typename?: 'IceCandidate';
  candidate: Scalars['String']['output'];
  from: Scalars['ID']['output'];
  sdpMLineIndex?: Maybe<Scalars['Int']['output']>;
  sdpMid?: Maybe<Scalars['String']['output']>;
  to: Scalars['ID']['output'];
};

export type InputSignup = {
  accountEmail?: InputMaybe<Scalars['String']['input']>;
};

export type Inv_SubImage = {
  __typename?: 'Inv_subImage';
  ImagePath?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  isVideo?: Maybe<Scalars['String']['output']>;
  subImageRelationChild?: Maybe<Scalars['String']['output']>;
  subImageRelationParent?: Maybe<Scalars['Int']['output']>;
};

export type Inventory = {
  __typename?: 'Inventory';
  agentEmail?: Maybe<Scalars['String']['output']>;
  brandname?: Maybe<Scalars['String']['output']>;
  category?: Maybe<Scalars['String']['output']>;
  childInventory?: Maybe<Array<Maybe<ChildInventory>>>;
  collectionItem?: Maybe<Scalars['String']['output']>;
  dateCreated?: Maybe<Scalars['String']['output']>;
  dateUpdated?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  name?: Maybe<Scalars['String']['output']>;
  productType?: Maybe<Scalars['String']['output']>;
  status?: Maybe<Scalars['String']['output']>;
  styleCode?: Maybe<Scalars['String']['output']>;
};

export type Ipadd = {
  __typename?: 'Ipadd';
  IpAddress?: Maybe<Scalars['String']['output']>;
};

export type Likes = {
  __typename?: 'Likes';
  accountEmail?: Maybe<Scalars['String']['output']>;
  dateCreated?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  productCode?: Maybe<Scalars['String']['output']>;
};

export type LikesParam = {
  accountEmail?: InputMaybe<Scalars['String']['input']>;
  productCode?: InputMaybe<Scalars['String']['input']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  ReadLogin?: Maybe<Result>;
  contactUs?: Maybe<Result>;
  createLinkToChangePassword?: Maybe<Result>;
  deleteAbout?: Maybe<Result>;
  deleteChildInventory?: Maybe<Result>;
  deleteDisclaimer?: Maybe<Result>;
  deleteNews?: Maybe<Result>;
  deletePrivacy?: Maybe<Result>;
  deleteShippingDetails?: Maybe<Result>;
  insertAbout?: Maybe<Result>;
  insertBrand?: Maybe<Result>;
  insertCategory?: Maybe<Result>;
  insertChildInventory?: Maybe<Result>;
  insertDisclaimer?: Maybe<Result>;
  insertFAQ?: Maybe<Result>;
  insertInventory?: Maybe<Result>;
  insertLikes?: Maybe<Result>;
  insertNews?: Maybe<Result>;
  insertNumberOfViews?: Maybe<Result>;
  insertNumberOfVisit?: Maybe<Result>;
  insertOrder?: Maybe<Result>;
  insertPrivacy?: Maybe<Result>;
  insertProductFeedBacks?: Maybe<Result>;
  insertProductTypes?: Maybe<Result>;
  insertShippingDetails?: Maybe<Result>;
  insertSignUp?: Maybe<Result>;
  insertUser?: Maybe<Result>;
  postMessage: CrowdMessages;
  postPersonalMessage: PersonalMessages;
  saveCropImage?: Maybe<Result>;
  scheduleOperation?: Maybe<Scalars['String']['output']>;
  setActiveUsers?: Maybe<Array<Maybe<ActivatedUsers>>>;
  startCall: Call;
  updateAccountDetails?: Maybe<Result>;
  updateChildInventory?: Maybe<Result>;
  updateDefaultAddress?: Maybe<Result>;
  updateNews?: Maybe<Result>;
  updateOrderStatusDelivered?: Maybe<Result>;
  updateOrderStatusDelivery?: Maybe<Result>;
  updateOrderStatusLogistic?: Maybe<Result>;
  updateOrderStatusPacked?: Maybe<Result>;
  updateOrderStatusRecieved?: Maybe<Result>;
  updateParentInventory?: Maybe<Result>;
  updatePassword?: Maybe<Result>;
  updateProductFeedBackStatus?: Maybe<Result>;
  upload3DModel?: Maybe<Result>;
  uploadCategoryImage?: Maybe<Result>;
};


export type MutationReadLoginArgs = {
  password?: InputMaybe<Scalars['String']['input']>;
  username?: InputMaybe<Scalars['String']['input']>;
};


export type MutationContactUsArgs = {
  messagebody?: InputMaybe<Messagebody>;
};


export type MutationCreateLinkToChangePasswordArgs = {
  emailAddress?: InputMaybe<Scalars['String']['input']>;
  path?: InputMaybe<Scalars['String']['input']>;
};


export type MutationDeleteAboutArgs = {
  id?: InputMaybe<Scalars['String']['input']>;
};


export type MutationDeleteChildInventoryArgs = {
  id?: InputMaybe<Scalars['String']['input']>;
};


export type MutationDeleteDisclaimerArgs = {
  id?: InputMaybe<Scalars['String']['input']>;
};


export type MutationDeleteNewsArgs = {
  param?: InputMaybe<Scalars['String']['input']>;
};


export type MutationDeletePrivacyArgs = {
  id?: InputMaybe<Scalars['String']['input']>;
};


export type MutationDeleteShippingDetailsArgs = {
  id?: InputMaybe<Scalars['String']['input']>;
};


export type MutationInsertAboutArgs = {
  content?: InputMaybe<Scalars['String']['input']>;
};


export type MutationInsertBrandArgs = {
  BrandnameInput?: InputMaybe<BrandnameInput>;
};


export type MutationInsertCategoryArgs = {
  CategoryInput?: InputMaybe<CategoryInput>;
};


export type MutationInsertChildInventoryArgs = {
  brandname?: InputMaybe<Scalars['String']['input']>;
  category?: InputMaybe<Scalars['String']['input']>;
  emailAddress?: InputMaybe<Scalars['String']['input']>;
  productColor?: InputMaybe<Scalars['String']['input']>;
  productDescription?: InputMaybe<Scalars['String']['input']>;
  productName?: InputMaybe<Scalars['String']['input']>;
  productPrice?: InputMaybe<Scalars['String']['input']>;
  productSize?: InputMaybe<Scalars['String']['input']>;
  productStock?: InputMaybe<Scalars['String']['input']>;
  productType?: InputMaybe<Scalars['String']['input']>;
  styleCode?: InputMaybe<Scalars['String']['input']>;
};


export type MutationInsertDisclaimerArgs = {
  content?: InputMaybe<Scalars['String']['input']>;
};


export type MutationInsertFaqArgs = {
  answer?: InputMaybe<Scalars['String']['input']>;
  question?: InputMaybe<Scalars['String']['input']>;
};


export type MutationInsertInventoryArgs = {
  brandname?: InputMaybe<Scalars['String']['input']>;
  category?: InputMaybe<Scalars['String']['input']>;
  emailAddress?: InputMaybe<Scalars['String']['input']>;
  productName?: InputMaybe<Scalars['String']['input']>;
  productType?: InputMaybe<Scalars['String']['input']>;
};


export type MutationInsertLikesArgs = {
  LikesParamInput?: InputMaybe<LikesParam>;
};


export type MutationInsertNewsArgs = {
  NewsInput?: InputMaybe<NewsInput>;
};


export type MutationInsertNumberOfViewsArgs = {
  Country?: InputMaybe<Scalars['String']['input']>;
  IpAddress?: InputMaybe<Scalars['String']['input']>;
  count?: InputMaybe<Scalars['String']['input']>;
  emailAddress?: InputMaybe<Scalars['String']['input']>;
  productCode?: InputMaybe<Scalars['String']['input']>;
};


export type MutationInsertNumberOfVisitArgs = {
  Country?: InputMaybe<Scalars['String']['input']>;
  IpAddress?: InputMaybe<Scalars['String']['input']>;
  emailAddress?: InputMaybe<Scalars['String']['input']>;
};


export type MutationInsertOrderArgs = {
  OrderHistoryInput?: InputMaybe<Array<InputMaybe<OrderHistoryInput>>>;
};


export type MutationInsertPrivacyArgs = {
  content?: InputMaybe<Scalars['String']['input']>;
};


export type MutationInsertProductFeedBacksArgs = {
  ProductFeedBacksInput?: InputMaybe<Array<InputMaybe<ProductFeedBacksInput>>>;
};


export type MutationInsertProductTypesArgs = {
  ProductTypesInput?: InputMaybe<ProductTypesInput>;
};


export type MutationInsertShippingDetailsArgs = {
  shippingDetailsInput?: InputMaybe<ShippingDetailsInput>;
};


export type MutationInsertSignUpArgs = {
  SignUpParameters?: InputMaybe<Array<InputMaybe<SignUpParameters>>>;
};


export type MutationInsertUserArgs = {
  agentIdentity?: InputMaybe<Scalars['String']['input']>;
  emailAddress?: InputMaybe<Scalars['String']['input']>;
  password?: InputMaybe<Scalars['String']['input']>;
};


export type MutationPostMessageArgs = {
  Message?: InputMaybe<Scalars['String']['input']>;
  Sender?: InputMaybe<Scalars['String']['input']>;
};


export type MutationPostPersonalMessageArgs = {
  Message?: InputMaybe<Scalars['String']['input']>;
  Reciever?: InputMaybe<Scalars['String']['input']>;
  Sender?: InputMaybe<Scalars['String']['input']>;
};


export type MutationSaveCropImageArgs = {
  file?: InputMaybe<Scalars['Upload']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
};


export type MutationScheduleOperationArgs = {
  name?: InputMaybe<Scalars['String']['input']>;
};


export type MutationSetActiveUsersArgs = {
  emailAddress?: InputMaybe<Scalars['String']['input']>;
};


export type MutationStartCallArgs = {
  callInput: CallInput;
};


export type MutationUpdateAccountDetailsArgs = {
  updateAccountDetailsInput?: InputMaybe<UpdateAccountDetailsInput>;
};


export type MutationUpdateChildInventoryArgs = {
  Email?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  productColor?: InputMaybe<Scalars['String']['input']>;
  productDescription?: InputMaybe<Scalars['String']['input']>;
  productPrice?: InputMaybe<Scalars['String']['input']>;
  productSize?: InputMaybe<Scalars['String']['input']>;
  productStatus?: InputMaybe<Scalars['String']['input']>;
  productStock?: InputMaybe<Scalars['String']['input']>;
};


export type MutationUpdateDefaultAddressArgs = {
  accountEmail?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
};


export type MutationUpdateNewsArgs = {
  NewsInput?: InputMaybe<NewsInput>;
};


export type MutationUpdateOrderStatusDeliveredArgs = {
  OrderstatusParameter?: InputMaybe<OrderstatusParameter>;
};


export type MutationUpdateOrderStatusDeliveryArgs = {
  OrderstatusParameter?: InputMaybe<OrderstatusParameter>;
};


export type MutationUpdateOrderStatusLogisticArgs = {
  OrderstatusParameter?: InputMaybe<OrderstatusParameter>;
};


export type MutationUpdateOrderStatusPackedArgs = {
  OrderstatusParameter?: InputMaybe<OrderstatusParameter>;
};


export type MutationUpdateOrderStatusRecievedArgs = {
  OrderstatusParameter?: InputMaybe<OrderstatusParameter>;
};


export type MutationUpdateParentInventoryArgs = {
  brandname?: InputMaybe<Scalars['String']['input']>;
  category?: InputMaybe<Scalars['String']['input']>;
  productID?: InputMaybe<Scalars['String']['input']>;
  productName?: InputMaybe<Scalars['String']['input']>;
  productType?: InputMaybe<Scalars['String']['input']>;
  status?: InputMaybe<Scalars['String']['input']>;
};


export type MutationUpdatePasswordArgs = {
  emailAddress?: InputMaybe<Scalars['String']['input']>;
  password?: InputMaybe<Scalars['String']['input']>;
};


export type MutationUpdateProductFeedBackStatusArgs = {
  ProductFeedBacksStatusParameter?: InputMaybe<Array<InputMaybe<OrderstatusParameter>>>;
};


export type MutationUpload3DModelArgs = {
  id?: InputMaybe<Scalars['String']['input']>;
  model?: InputMaybe<Scalars['Upload']['input']>;
};


export type MutationUploadCategoryImageArgs = {
  id?: InputMaybe<Scalars['String']['input']>;
  image?: InputMaybe<Scalars['Upload']['input']>;
};

export type News = {
  __typename?: 'News';
  dateCreated?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  postedBy?: Maybe<Scalars['String']['output']>;
  summary?: Maybe<Scalars['String']['output']>;
  thumbnail?: Maybe<Scalars['String']['output']>;
  title?: Maybe<Scalars['String']['output']>;
};

export type NewsInput = {
  postedBy?: InputMaybe<Scalars['String']['input']>;
  summary?: InputMaybe<Scalars['String']['input']>;
  thumbnail?: InputMaybe<Scalars['String']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
};

export type NumberOfViews = {
  __typename?: 'NumberOfViews';
  Country?: Maybe<Scalars['String']['output']>;
  IpAddress?: Maybe<Scalars['String']['output']>;
  count?: Maybe<Scalars['String']['output']>;
  dateVisited?: Maybe<Scalars['String']['output']>;
  emailAddress?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  productCode?: Maybe<Scalars['String']['output']>;
};

export type Operation = {
  __typename?: 'Operation';
  endDate?: Maybe<Scalars['String']['output']>;
  name?: Maybe<Scalars['String']['output']>;
};

export type OrderHistory = {
  __typename?: 'OrderHistory';
  Address?: Maybe<Scalars['String']['output']>;
  Color?: Maybe<Scalars['String']['output']>;
  Contact?: Maybe<Scalars['String']['output']>;
  Image?: Maybe<Scalars['String']['output']>;
  OrderNo?: Maybe<Scalars['String']['output']>;
  OrderStatus?: Maybe<Scalars['String']['output']>;
  Price?: Maybe<Scalars['Float']['output']>;
  Quantity?: Maybe<Scalars['Int']['output']>;
  Size?: Maybe<Scalars['String']['output']>;
  StatusText?: Maybe<Scalars['String']['output']>;
  StoreEmail?: Maybe<Scalars['String']['output']>;
  TrackingNo?: Maybe<Scalars['String']['output']>;
  agentEmail?: Maybe<Scalars['String']['output']>;
  dateCreated?: Maybe<Scalars['String']['output']>;
  emailAddress?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  paymentMethod?: Maybe<Scalars['String']['output']>;
  productCode?: Maybe<Scalars['String']['output']>;
};

export type OrderHistoryInput = {
  Address?: InputMaybe<Scalars['String']['input']>;
  Color?: InputMaybe<Scalars['String']['input']>;
  Contact?: InputMaybe<Scalars['String']['input']>;
  Image?: InputMaybe<Scalars['String']['input']>;
  OrderNo?: InputMaybe<Scalars['String']['input']>;
  Price?: InputMaybe<Scalars['Float']['input']>;
  Quantity?: InputMaybe<Scalars['Int']['input']>;
  Size?: InputMaybe<Scalars['String']['input']>;
  TrackingNo?: InputMaybe<Scalars['String']['input']>;
  agentEmail?: InputMaybe<Scalars['String']['input']>;
  emailAddress?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  paymentMethod?: InputMaybe<Scalars['String']['input']>;
  productCode?: InputMaybe<Scalars['String']['input']>;
};

export type OrderHistory_Total_Quantity = {
  __typename?: 'OrderHistory_total_quantity';
  OrderStatus?: Maybe<Scalars['String']['output']>;
  Quantity?: Maybe<Scalars['Int']['output']>;
};

export type OrderstatusParameter = {
  OrderNo?: InputMaybe<Scalars['String']['input']>;
  agentEmail?: InputMaybe<Scalars['String']['input']>;
};

export type PersonalMessages = {
  __typename?: 'PersonalMessages';
  Messages?: Maybe<Scalars['String']['output']>;
  Reciever?: Maybe<Scalars['String']['output']>;
  Sender?: Maybe<Scalars['String']['output']>;
  dateSent?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
};

export type Privacy = {
  __typename?: 'Privacy';
  content?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
};

export type ProductFeedBacks = {
  __typename?: 'ProductFeedBacks';
  Attachment?: Maybe<Scalars['String']['output']>;
  By?: Maybe<Scalars['String']['output']>;
  Comment?: Maybe<Scalars['String']['output']>;
  OrderNo?: Maybe<Scalars['String']['output']>;
  Ratings?: Maybe<Scalars['Float']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  productCode?: Maybe<Scalars['String']['output']>;
};

export type ProductFeedBacksInput = {
  Attachment?: InputMaybe<Scalars['Upload']['input']>;
  By?: InputMaybe<Scalars['String']['input']>;
  Comment?: InputMaybe<Scalars['String']['input']>;
  OrderNo?: InputMaybe<Scalars['String']['input']>;
  Ratings?: InputMaybe<Scalars['Float']['input']>;
  productCode?: InputMaybe<Scalars['String']['input']>;
};

export type ProductTypes = {
  __typename?: 'ProductTypes';
  Category?: Maybe<Scalars['String']['output']>;
  Name?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
};

export type ProductTypesInput = {
  Category?: InputMaybe<Scalars['String']['input']>;
  Name?: InputMaybe<Scalars['String']['input']>;
};

export type Query = {
  __typename?: 'Query';
  getAccountDetails?: Maybe<Array<Maybe<AccountDetails>>>;
  getAccountDetails_id?: Maybe<Array<Maybe<AccountDetails>>>;
  getBrand?: Maybe<Array<Maybe<Brandname>>>;
  getCategory?: Maybe<Array<Maybe<Category>>>;
  getChildInventory?: Maybe<Array<Maybe<ChildInventory>>>;
  getChildInventory_details?: Maybe<Array<Maybe<ChildInventory>>>;
  getFilteredUser?: Maybe<Array<Maybe<User>>>;
  getGroupedOrderHistory?: Maybe<Array<Maybe<GroupedOrders>>>;
  getGroupedOrderHistoryDelivered?: Maybe<Array<Maybe<GroupedOrders>>>;
  getGroupedOrderHistoryDelivery?: Maybe<Array<Maybe<GroupedOrders>>>;
  getGroupedOrderHistoryLogistic?: Maybe<Array<Maybe<GroupedOrders>>>;
  getGroupedOrderHistoryPacked?: Maybe<Array<Maybe<GroupedOrders>>>;
  getGroupedOrderHistoryRecieved?: Maybe<Array<Maybe<GroupedOrders>>>;
  getInv_subImage?: Maybe<Array<Maybe<Inv_SubImage>>>;
  getIp?: Maybe<Array<Maybe<Ipadd>>>;
  getLogin?: Maybe<Result>;
  getNameofStore?: Maybe<Array<Maybe<User>>>;
  getNumberOfViews?: Maybe<Array<Maybe<NumberOfViews>>>;
  getOrderHistory?: Maybe<Array<Maybe<OrderHistory>>>;
  getParentInventory?: Maybe<Array<Maybe<Inventory>>>;
  getProductTypes?: Maybe<Array<Maybe<ProductTypes>>>;
  getRelatedProduct?: Maybe<Array<Maybe<ChildInventory>>>;
  getToviewProduct?: Maybe<Array<Maybe<ChildInventory>>>;
  getUser?: Maybe<Array<Maybe<User>>>;
  getWebsitVisits?: Maybe<Array<Maybe<WebsiteVisits>>>;
  messages?: Maybe<Array<Maybe<CrowdMessages>>>;
  personalMessages?: Maybe<Array<Maybe<PersonalMessages>>>;
  readAbout?: Maybe<Array<Maybe<About>>>;
  readActiveUsers?: Maybe<Array<Maybe<ActivatedUsers>>>;
  readConversationist?: Maybe<Array<Maybe<PersonalMessages>>>;
  readDisclaimer?: Maybe<Array<Maybe<Disclaimer>>>;
  readFAQ?: Maybe<Array<Maybe<Faq>>>;
  readFeedBack?: Maybe<Array<Maybe<ProductFeedBacks>>>;
  readGroupSender?: Maybe<Array<Maybe<PersonalMessages>>>;
  readGroupedOrderHistory?: Maybe<Array<Maybe<GroupedOrders>>>;
  readGroupedOrderHistoryDelivered?: Maybe<Array<Maybe<GroupedOrders>>>;
  readGroupedOrderHistoryDelivery?: Maybe<Array<Maybe<GroupedOrders>>>;
  readGroupedOrderHistoryLogistic?: Maybe<Array<Maybe<GroupedOrders>>>;
  readGroupedOrderHistoryPacked?: Maybe<Array<Maybe<GroupedOrders>>>;
  readGroupedOrderHistoryRecieved?: Maybe<Array<Maybe<GroupedOrders>>>;
  readLikes?: Maybe<Array<Maybe<ChildInventory>>>;
  readNews?: Maybe<Array<Maybe<News>>>;
  readNewsManagement?: Maybe<Array<Maybe<News>>>;
  readNewsPoster?: Maybe<Array<Maybe<News>>>;
  readPrivacy?: Maybe<Array<Maybe<Privacy>>>;
  readRatings?: Maybe<Scalars['String']['output']>;
  readSales?: Maybe<Array<Maybe<SortedSales>>>;
  readStatistics?: Maybe<Array<Maybe<SortedSales>>>;
};


export type QueryGetAccountDetails_IdArgs = {
  id?: InputMaybe<Scalars['String']['input']>;
};


export type QueryGetChildInventory_DetailsArgs = {
  styleCode?: InputMaybe<Scalars['String']['input']>;
};


export type QueryGetFilteredUserArgs = {
  UserLevel?: InputMaybe<Scalars['String']['input']>;
  emailAddress?: InputMaybe<Scalars['String']['input']>;
};


export type QueryGetGroupedOrderHistoryArgs = {
  emailAddress?: InputMaybe<Scalars['String']['input']>;
};


export type QueryGetGroupedOrderHistoryDeliveredArgs = {
  emailAddress?: InputMaybe<Scalars['String']['input']>;
};


export type QueryGetGroupedOrderHistoryDeliveryArgs = {
  emailAddress?: InputMaybe<Scalars['String']['input']>;
};


export type QueryGetGroupedOrderHistoryLogisticArgs = {
  emailAddress?: InputMaybe<Scalars['String']['input']>;
};


export type QueryGetGroupedOrderHistoryPackedArgs = {
  emailAddress?: InputMaybe<Scalars['String']['input']>;
};


export type QueryGetGroupedOrderHistoryRecievedArgs = {
  emailAddress?: InputMaybe<Scalars['String']['input']>;
};


export type QueryGetIpArgs = {
  ipAddress?: InputMaybe<Scalars['String']['input']>;
};


export type QueryGetLoginArgs = {
  password?: InputMaybe<Scalars['String']['input']>;
  username?: InputMaybe<Scalars['String']['input']>;
};


export type QueryGetOrderHistoryArgs = {
  emailAddress?: InputMaybe<Scalars['String']['input']>;
};


export type QueryGetParentInventoryArgs = {
  EmailAddress?: InputMaybe<Scalars['String']['input']>;
};


export type QueryGetToviewProductArgs = {
  id?: InputMaybe<Scalars['String']['input']>;
};


export type QueryPersonalMessagesArgs = {
  emailAddress?: InputMaybe<Scalars['String']['input']>;
  reciever?: InputMaybe<Scalars['String']['input']>;
};


export type QueryReadActiveUsersArgs = {
  emailAddress?: InputMaybe<Scalars['String']['input']>;
};


export type QueryReadConversationistArgs = {
  emailAddress?: InputMaybe<Scalars['String']['input']>;
};


export type QueryReadGroupSenderArgs = {
  emailAddress?: InputMaybe<Scalars['String']['input']>;
};


export type QueryReadGroupedOrderHistoryArgs = {
  emailAddress?: InputMaybe<Scalars['String']['input']>;
};


export type QueryReadGroupedOrderHistoryDeliveredArgs = {
  emailAddress?: InputMaybe<Scalars['String']['input']>;
};


export type QueryReadGroupedOrderHistoryDeliveryArgs = {
  emailAddress?: InputMaybe<Scalars['String']['input']>;
};


export type QueryReadGroupedOrderHistoryLogisticArgs = {
  emailAddress?: InputMaybe<Scalars['String']['input']>;
};


export type QueryReadGroupedOrderHistoryPackedArgs = {
  emailAddress?: InputMaybe<Scalars['String']['input']>;
};


export type QueryReadGroupedOrderHistoryRecievedArgs = {
  emailAddress?: InputMaybe<Scalars['String']['input']>;
};


export type QueryReadLikesArgs = {
  accountEmail?: InputMaybe<Scalars['String']['input']>;
  productCode?: InputMaybe<Scalars['String']['input']>;
};


export type QueryReadNewsManagementArgs = {
  emailAddress?: InputMaybe<Scalars['String']['input']>;
};


export type QueryReadRatingsArgs = {
  productCodes?: InputMaybe<Scalars['String']['input']>;
};


export type QueryReadSalesArgs = {
  emailAddress?: InputMaybe<Scalars['String']['input']>;
  level?: InputMaybe<Scalars['String']['input']>;
  period?: InputMaybe<Scalars['String']['input']>;
};


export type QueryReadStatisticsArgs = {
  period?: InputMaybe<Scalars['String']['input']>;
};

export type Result = {
  __typename?: 'Result';
  jsonToken?: Maybe<Scalars['String']['output']>;
  statusText?: Maybe<Scalars['String']['output']>;
};

export type SignUpParameters = {
  PassWord?: InputMaybe<Scalars['String']['input']>;
  contactNo?: InputMaybe<Scalars['String']['input']>;
  emailAddress?: InputMaybe<Scalars['String']['input']>;
  fullname?: InputMaybe<Scalars['String']['input']>;
};

export type SortedSales = {
  __typename?: 'SortedSales';
  Interval?: Maybe<Scalars['String']['output']>;
  orderHistory?: Maybe<Array<Maybe<OrderHistory>>>;
  totalSales?: Maybe<Scalars['Float']['output']>;
};

export type Subscription = {
  __typename?: 'Subscription';
  ActiveUserList?: Maybe<Array<Maybe<ActiveUsers>>>;
  callReceived: Call;
  iceCandidateReceived: IceCandidate;
  messageAdded?: Maybe<CrowdMessages>;
  messageNews?: Maybe<Array<Maybe<News>>>;
  messageToOrder?: Maybe<Array<Maybe<OrderHistory>>>;
  messagesPersonal?: Maybe<Array<Maybe<PersonalMessages>>>;
};

export type UpdatePasswordInput = {
  emailAddress?: InputMaybe<Scalars['String']['input']>;
  newPassword?: InputMaybe<Scalars['String']['input']>;
};

export type User = {
  __typename?: 'User';
  accountCode?: Maybe<Scalars['String']['output']>;
  accountDetails?: Maybe<Array<Maybe<AccountDetails>>>;
  accountLevel?: Maybe<Scalars['String']['output']>;
  agentIdentity?: Maybe<Scalars['String']['output']>;
  dateCreated?: Maybe<Scalars['String']['output']>;
  dateUpdated?: Maybe<Scalars['String']['output']>;
  email?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  image?: Maybe<Scalars['String']['output']>;
  loginAttemp?: Maybe<Scalars['String']['output']>;
  macAddress?: Maybe<Scalars['String']['output']>;
  nameOfStore?: Maybe<Scalars['String']['output']>;
  password?: Maybe<Scalars['String']['output']>;
};

export type WebsiteVisits = {
  __typename?: 'WebsiteVisits';
  Country?: Maybe<Scalars['String']['output']>;
  IpAddress?: Maybe<Scalars['String']['output']>;
  dateVisited?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
};

export type Messagebody = {
  contactNo?: InputMaybe<Scalars['String']['input']>;
  details?: InputMaybe<Scalars['String']['input']>;
  emailAddress?: InputMaybe<Scalars['String']['input']>;
  fullname?: InputMaybe<Scalars['String']['input']>;
};

export type ShippingDetailsInput = {
  Address?: InputMaybe<Scalars['String']['input']>;
  accountEmail?: InputMaybe<Scalars['String']['input']>;
  contactNo?: InputMaybe<Scalars['String']['input']>;
  fullname?: InputMaybe<Scalars['String']['input']>;
  userId?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateAccountDetailsInput = {
  accountDetails?: InputMaybe<UpdateAccountDetailsInputInner>;
  accountLevel?: InputMaybe<Scalars['String']['input']>;
  agentIdentity?: InputMaybe<Scalars['String']['input']>;
  email?: InputMaybe<Scalars['String']['input']>;
  loginAttemp?: InputMaybe<Scalars['String']['input']>;
  macAddress?: InputMaybe<Scalars['String']['input']>;
  nameOfStore?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateAccountDetailsInputInner = {
  Address?: InputMaybe<Scalars['String']['input']>;
  accountEmail?: InputMaybe<Scalars['String']['input']>;
  contactNo?: InputMaybe<Scalars['String']['input']>;
  defaultAddress?: InputMaybe<Scalars['String']['input']>;
  fullname?: InputMaybe<Scalars['String']['input']>;
  storeName?: InputMaybe<Scalars['String']['input']>;
  userId?: InputMaybe<Scalars['String']['input']>;
};
