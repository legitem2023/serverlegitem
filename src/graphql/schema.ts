

import { gql } from 'graphql-tag'

export const typeDefs = gql`
scalar Upload
scalar Date
scalar Buffer
scalar JSON
type User {
    id:String
    email:String
    accountCode:String
    accountDetails:[AccountDetails]
    password:String
    accountLevel:String
    loginAttemp:String
    macAddress:String
    agentIdentity:String
    image:String
    dateCreated:String
    dateUpdated: String
    nameOfStore: String
  }
type AccountDetails {
  id: String
  userId: String
  fullname: String
  storeName: String
  contactNo: String
  Address: String
  accountEmail: String
  defaultAddress:Boolean
}

type CrowdMessages {
  id:String
  Messages:String
  Sender:String
  Live:String
  Video:String
  dateSent:String
}

type Comments {
  id:String
  Messages:String
  Sender:String
  dateSent:String
}

type PersonalMessages {
  id:String
  Messages:String
  Sender:String
  Reciever:String
  dateSent:String
}

type News {
  id:String
  title:String
  thumbnail:String
  summary:String
  postedBy: String
  dateCreated:String
}

type ActiveUsers {
  fullname: String
  accountEmail: String
}


type Category{
  id:String
  Name:String
  status:String
  icon:String
  image:String
}

type ProductTypes{
  id:String
  Category:String
  Name:String
}

type Brandname{
  id:String
  ProductType:String
  Name:String
}


type Inventory {
  id:String
  styleCode:String
  productType:String
  category:String
  name:String
  status:String
  dateCreated:String
  dateUpdated:String
  agentEmail:String
  brandname:String
  collectionItem:String
  childInventory:[ChildInventory]
}

type ChildInventory {
  id:String
  productCode:String
  category:String
  productType:String
  brandname:String
  imageReferences:String
  style_Code:String
  name:String
  color:String
  size:String
  price:String
  stock:String
  discount:Float
  status:String
  thumbnail:String
  parentId:String
  creator:String
  editor:String
  dateCreated:String
  dateUpdated:String
  agentEmail:String
  subImageFieldOut:[Inv_subImage]
  Ratings:[ProductFeedBacks]
  Views:[NumberOfViews]
  Likes:[Likes]
  SoldItems:[OrderHistory_total_quantity]
  paymentMethod:String
  model: String
  productDescription: String
  TotalSoldItems:Int
  TotalRatings:Float
}

type OrderHistory_total_quantity {
  Quantity:Int
  OrderStatus:String
}

type OrderHistory {
  id:String
  Image:String
  Size:String
  Color:String
  productCode:String
  emailAddress:String
  TrackingNo:String
  OrderNo:String
  Quantity:Int
  Price:Float
  Address:String
  Contact:String
  StoreEmail:String
  dateCreated:String
  agentEmail:String
  StatusText:String
  OrderStatus:String
  paymentMethod:String
}

type Inv_subImage {
  id:String
  subImageRelationParent:Int
  subImageRelationChild:String
  ImagePath:String
  isVideo:String
}

type NumberOfViews {
  id:String
  count:String
  productCode:String
  emailAddress:String
  IpAddress:String
  Country:String
  dateVisited:String
}

type WebsiteVisits {
  id:String
  IpAddress:String
  Country:String
  dateVisited:String
}

input InputSignup {
  accountEmail:String
}

type Result{
  statusText:String
  jsonToken:String
}

type Ipadd{
  IpAddress:String
}

input OrderHistoryInput {
  id:String
  Image:String
  Size:String
  Color:String
  productCode:String
  emailAddress:String
  TrackingNo:String
  OrderNo:String
  Quantity:Int
  Price:Float
  Address:String
  Contact:String
  agentEmail:String
  paymentMethod:String
}

type GroupedOrders {
  OrderNo:String
  Address:String
  Contact:String
  StatusText:String
  OrderStatus:String
  OrderHistory:[OrderHistory]
}

input SignUpParameters {
  fullname:String
  contactNo:String
  emailAddress:String
  PassWord:String
}

input NewsInput {
  title:String
  thumbnail:String
  summary:String
  postedBy: String
}

type ActivatedUsers {
  accountEmail: String,
  fullname: String
}

type Privacy {
    id:String
    content:String
}

type Disclaimer {
  id:String
  content:String
}

type About {
  id:String
  content:String
}

type FAQ {
  id:String
  question:String
  answer:String
}

type ProductFeedBacks {
  id:String
  productCode:String
  OrderNo:String
  Ratings:Float
  Attachment:String
  Comment:String
  By:String
}

type Likes {
  id: String
  productCode:String
  accountEmail:String
  dateCreated:String
}

type SortedSales {
  Interval: String
  totalSales: Float
  orderHistory: [OrderHistory]
}


type Signal {
    type: String!
    sender: ID!
    receiver: ID!
    payload: JSON!
    }

input SignalInput {
    type: String!
    sender: ID!
    receiver: ID!
    payload: JSON!
}


type Query {
  readSales(period:String,emailAddress:String,level:String): [SortedSales]
  readStatistics(period:String): [SortedSales]
  
  readLikes(accountEmail:String,productCode:String): [ChildInventory]
  readFeedBack:[ProductFeedBacks]
  readRatings(productCodes:String):String
  readPrivacy: [Privacy]
  readDisclaimer: [Disclaimer]
  readAbout: [About]
  readFAQ: [FAQ]
  readConversationist(emailAddress:String): [PersonalMessages]
  readActiveUsers(emailAddress:String): [ActivatedUsers]
  readNewsManagement(emailAddress:String): [News]
  readNews: [News]
  readNewsPoster:[News]
  readGroupedOrderHistory(emailAddress:String):[GroupedOrders]
  readGroupedOrderHistoryRecieved(emailAddress:String):[GroupedOrders]
  readGroupedOrderHistoryPacked(emailAddress:String):[GroupedOrders]
  readGroupedOrderHistoryLogistic(emailAddress:String):[GroupedOrders]
  readGroupedOrderHistoryDelivery(emailAddress:String):[GroupedOrders]
  readGroupedOrderHistoryDelivered(emailAddress:String):[GroupedOrders]

  getGroupedOrderHistory(emailAddress:String):[GroupedOrders]
  getGroupedOrderHistoryRecieved(emailAddress:String):[GroupedOrders]
  getGroupedOrderHistoryPacked(emailAddress:String):[GroupedOrders]
  getGroupedOrderHistoryLogistic(emailAddress:String):[GroupedOrders]
  getGroupedOrderHistoryDelivery(emailAddress:String):[GroupedOrders]
  getGroupedOrderHistoryDelivered(emailAddress:String):[GroupedOrders]

  getOrderHistory(emailAddress:String):[OrderHistory]
  getUser: [User]
  getFilteredUser(UserLevel:String,emailAddress:String):[User]
  getIp(ipAddress:String):[Ipadd]
  getAccountDetails: [AccountDetails]
  getAccountDetails_id(id:String):[AccountDetails]
  getParentInventory(EmailAddress:String):[Inventory]
  getChildInventory:[ChildInventory]
  getChildInventory_details(styleCode:String):[ChildInventory]
  getCategory:[Category]
  getProductTypes:[ProductTypes]
  getBrand:[Brandname]
  getRelatedProduct:[ChildInventory]
  getToviewProduct(id:String):[ChildInventory]
  getLogin(username:String,password:String):Result
  getNameofStore:[User]
  getNumberOfViews:[NumberOfViews]
  getWebsitVisits:[WebsiteVisits]
  getInv_subImage:[Inv_subImage]
  getComment(id:String):[Comments]
  messages:[CrowdMessages]
  personalMessages(emailAddress:String,reciever:String):[PersonalMessages]
  readGroupSender(emailAddress:String):[PersonalMessages]
  streams: [Stream!]!
}
input OrderstatusParameter {
  OrderNo:String
  agentEmail:String
}

input UpdatePasswordInput {
  emailAddress:String
  newPassword:String
}

input messagebody {
  fullname:String
  emailAddress:String
  contactNo:String
  details:String
}

input updateAccountDetailsInputInner {
  Address:String,
  accountEmail:String,
  contactNo:String,
  defaultAddress:String,
  fullname:String,
  storeName:String,
  userId:String,
}

input updateAccountDetailsInput {
  accountDetails: updateAccountDetailsInputInner,
  accountLevel:String,
  agentIdentity:String,
  email:String,
  loginAttemp:String,
  macAddress:String,
  nameOfStore:String
}
input shippingDetailsInput {
  userId:String
  accountEmail:String,
  fullname:String,
  contactNo:String,
  Address:String,
}

input ProductFeedBacksInput {
  productCode:String
  OrderNo:String
  Ratings:Float
  Attachment:Upload
  Comment:String
  By:String
}

input LikesParam {
  productCode:String
  accountEmail:String
}

input CategoryInput{
  Name:String
  status:String
  icon:String
  image:Upload
}

input BrandnameInput {
  ProductType:String
  Name:String
}

input ProductTypesInput {
  Category:String
  Name:String
}

type Call {
    from: User!
    to: User!
}

input CallInput {
    from: ID!
    to: ID!
}

type UserTypingResponse {
  senderEmail: String!
  receiverEmail: String!
  isTyping: Boolean!
}


type Mutation {
  setUserTyping(senderEmail: String!, receiverEmail: String!, isTyping: Boolean!): UserTypingResponse!
  sendSignal(signal: SignalInput!): Signal
  startCall(callInput: CallInput!): Call!
  uploadCategoryImage(id:String,image:Upload):Result
  upload3DModel(id:String,model:Upload):Result

  insertCategory(CategoryInput:CategoryInput):Result
  insertBrand(BrandnameInput:BrandnameInput):Result
  insertProductTypes(ProductTypesInput:ProductTypesInput):Result
  insertLikes(LikesParamInput:LikesParam):Result
  insertProductFeedBacks(ProductFeedBacksInput:[ProductFeedBacksInput]):Result
  insertPrivacy(content:String):Result
  insertDisclaimer(content:String):Result
  insertAbout(content:String):Result
  insertFAQ(question:String,answer:String):Result

  deletePrivacy(id:String):Result
  deleteDisclaimer(id:String):Result
  deleteAbout(id:String):Result


  setActiveUsers(emailAddress:String): [ActivatedUsers]
  postPersonalMessage(Message:String,Sender:String,Reciever:String):PersonalMessages!

  updateProductFeedBackStatus(ProductFeedBacksStatusParameter:[OrderstatusParameter]):Result

  updateOrderStatusRecieved(OrderstatusParameter:OrderstatusParameter):Result
  updateOrderStatusPacked(OrderstatusParameter:OrderstatusParameter):Result
  updateOrderStatusLogistic(OrderstatusParameter:OrderstatusParameter):Result
  updateOrderStatusDelivery(OrderstatusParameter:OrderstatusParameter):Result
  updateOrderStatusDelivered(OrderstatusParameter:OrderstatusParameter):Result
  updatePassword(emailAddress:String,password:String):Result
  updateChildInventory(productColor:String,
                       productSize:String,
                       productPrice:String,
                       productStatus:String,
                       productStock:String,
                       productDescription:String,
                       id:String,
                       Email:String):Result
  updateParentInventory(productID:String,category:String,productType:String,brandname:String,productName:String,status:String):Result

  updateDefaultAddress(accountEmail:String,id:String):Result

  updateAccountDetails(updateAccountDetailsInput:updateAccountDetailsInput):Result

  insertShippingDetails(shippingDetailsInput:shippingDetailsInput):Result

  deleteShippingDetails(id:String):Result


  insertOrder(OrderHistoryInput:[OrderHistoryInput]):Result
  insertNumberOfVisit(emailAddress:String,IpAddress:String,Country:String):Result
  insertNumberOfViews(count:String,productCode:String,emailAddress:String,IpAddress:String,Country:String):Result
  insertUser(emailAddress:String,password:String,agentIdentity:String):Result

  insertInventory(emailAddress:String,
                  category:String,
                  productType:String,
                  brandname:String,
                  productName:String):Result
  insertChildInventory(emailAddress:String,
                       productStock:String,
                       productDescription:String,
                       productPrice:String,
                       productColor:String,
                       productSize:String,
                       category:String,
                       productType:String,
                       brandname:String,
                       productName:String,
                       styleCode:String):Result

  contactUs(messagebody:messagebody):Result
  createLinkToChangePassword(emailAddress:String,path:String):Result
  saveCropImage(id:String,file:Upload):Result
  scheduleOperation(name:String):String
  postMessage(Message:String,Sender:String,Live:String,Video:String):CrowdMessages
  live(Message:String,Sender:String,Live:String,Video:String):CrowdMessages

  postComment(Message:String,Sender:String,CrowID:String):Comments

  ReadLogin(username:String,password:String):Result
  insertSignUp(SignUpParameters:[SignUpParameters]):Result
  insertNews(NewsInput:NewsInput):Result
  updateNews(NewsInput:NewsInput):Result
  deleteNews(param:String):Result
  deleteChildInventory(id:String):Result
  createOffer(offer: String!): Boolean
  createAnswer(answer: String!): Boolean
  sendIceCandidate(candidate: String!): Boolean
}
type Stream {
    id: ID!
    title: String!
}


type Operation {
  name:String
  endDate:String
}



type TypingStatus {
    senderEmail: String!
    receiverEmail: String!
    isTyping: Boolean!
}



type Subscription {
  offer: String!
  answer: String!
  iceCandidate: String!
  userTyping(senderEmail: String!, receiverEmail: String!): TypingStatus
  messageAdded:CrowdMessages
  messageComments:Comments
signalCreated(userId: ID!): Signal
  messageToOrder:[OrderHistory]
  messageNews:[News]
  ActiveUserList:[ActiveUsers]
  messagesPersonal:[PersonalMessages]
  callReceived: Call!
}`;