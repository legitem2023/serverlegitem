export default {
    introspection: {
      type: 'sdl',
      paths: ['./src/graphql/schema.ts'],
    },
    website: {
      template: 'carbon-multi-page',
      options: {
        appTitle: 'Legitem Back-End',
        appLogo: '',
        pages: [{
          title: 'Page 01',
          content: `
  # Medium Article
  This is my article of magidoc.
  ## Where to go next?
  - Star the project on [GitHub](https://github.com/magidoc-org/magidoc) 
  `
        }],
      },
    },
  }